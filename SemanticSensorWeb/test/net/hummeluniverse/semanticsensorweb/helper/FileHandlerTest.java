/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.helper;

import static org.junit.Assert.*;

import java.io.Reader;

import org.junit.Test;

/**
 * @author Thomas Hummel
 * @since 23.04.2012
 *
 */
public class FileHandlerTest {
	//fields
	
	//constructor

	//basic methods

	//other methods

	
	/**
	 * Test method for {@link net.hummeluniverse.semanticsensorweb.helper.FileHandler#readLocalFile(java.lang.String)}.
	 */
	@Test
	public void testReadLocalFile() {
		Reader returnedReader = null;
		String pathToLocalFile = "files/test/basic.csv";
		
		returnedReader = FileHandler.readLocalFile(pathToLocalFile);
		
		assertNotNull("returned Reader Object should not be null", returnedReader);
	}

	

	/**
	 * Test method for {@link net.hummeluniverse.semanticsensorweb.helper.FileHandler#readRemoteFile(java.lang.String)}.
	 */
	@Test
	public void testReadRemoteFile() {
		Reader returnedReader = null;
		String urlToRemoteFile = "http://www.hummel-universe.net/studium/semanticsensorweb/files/test/basic.csv";
		
		returnedReader = FileHandler.readRemoteFile(urlToRemoteFile);
		
		assertNotNull("returned Reader Object should not be null", returnedReader);
	}


}
