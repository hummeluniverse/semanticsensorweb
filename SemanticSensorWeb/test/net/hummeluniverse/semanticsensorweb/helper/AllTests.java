/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.helper;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Thomas Hummel
 * @since 13.05.2012
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ FileHandlerTest.class })
public class AllTests {
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */

}
