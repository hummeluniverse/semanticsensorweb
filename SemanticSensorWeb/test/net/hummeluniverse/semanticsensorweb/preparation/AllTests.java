/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Thomas Hummel
 * @since 13.05.2012
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ CsvReaderTest.class, CsvSourceManagerTest.class,
		RDFCreatorTest.class })
public class AllTests {
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */

}
