/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import static org.junit.Assert.*;

import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Thomas Hummel
 * @since 16.04.2012
 *
 */
public class CsvReaderTest {
	
	//constructor
	@Test
	public void testCsvReader() {
		
		
	}
	
	//basic methods
	
	//other methods
	@Test
	public void testConvertToList() {
		//configure Reader
		Reader inputReader;
		
		//configure Testfiles
		String localCSV = "files/test/basic.csv";
		String localCSV2 = "files/test/basic2.csv"; //different separators and quotechars and additional blank line
		String remoteCSV = "http://www.hummel-universe.net/studium/semanticsensorweb/files/test/basic.csv";
//		String remoteCSV = "http://www.aviationweather.gov/adds/dataserver_current/current/aircraftreports.cache.csv";
		
		
		String[] expectedStringArr = {"aaaa", "bbbb", "cccc", "dddd"}; //first line
		String[] expectedStringArr2 = {"1111", "2222", "3333", "4444"}; //second line
		
		List<String[]> returnedList;

		//test local CSV
		inputReader = CsvReader.readLocalFile(localCSV);
		returnedList = CsvReader.convertToList(inputReader);
		
		assertTrue("First Line of returned list doesn't match expected String[]", Arrays.equals(expectedStringArr, returnedList.get(0)));
		assertEquals("Returned List should contain two String-Arrays",2, returnedList.size());
		
		//test remote CSV
		inputReader = CsvReader.readRemoteFile(remoteCSV);
		returnedList = CsvReader.convertToList(inputReader);
				
		assertTrue("First Line of returned list doesn't match expected String[]", Arrays.equals(expectedStringArr, returnedList.get(0)));
		assertEquals("Returned List should contain two String-Arrays",2, returnedList.size());
		
		//test line handling
		inputReader = CsvReader.readLocalFile(localCSV);
		returnedList = CsvReader.convertToList(inputReader,',', '"', 1);
				
		assertTrue("First Line of returned list (\"" + returnedList.get(0)[0] + "\") doesn't match expected String[] (\"" + expectedStringArr2[0] + "\")", Arrays.equals(expectedStringArr2, returnedList.get(0)));
		assertEquals("Returned List should contain one String-Array",1, returnedList.size());
		
		//test different separator and quotechars
		inputReader = CsvReader.readLocalFile(localCSV2);
		returnedList = CsvReader.convertToList(inputReader, ';', '\'', 0);

		assertTrue("First Line of returned list doesn't match expected String[]", Arrays.equals(expectedStringArr, returnedList.get(0)));
		assertEquals("Returned List should contain one String-Array",3, returnedList.size());
	}

}
