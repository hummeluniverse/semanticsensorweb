/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.Test;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author Thomas Hummel
 * @since 12.05.2012
 *
 */
public class CsvSourceManagerTest {

	/* constructor */

	/**
	 * Test method for {@link net.hummeluniverse.semanticsensorweb.preparation.CsvSourceManager#CsvSourceManager(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCsvSourceManager() {
		CsvSourceManager manager = new CsvSourceManager("Csv Test Source", true, "files/test/basic.csv");
	
		assertTrue(manager.getNameOfSource().equals("Csv Test Source"));
		assertTrue(manager.getLocalFile());
		assertTrue(manager.getPathToFile().equals("files/test/basic.csv"));
		
	}
	
	/* basic methods */
	
	
	/* other methods */

	/**
	 * Test method for {@link net.hummeluniverse.semanticsensorweb.preparation.CsvSourceManager#readConfigurationFile(java.lang.String, java.lang.Boolean)}.
	 */
	@Test
	public void testReadConfigurationFile() {
		String pathToTestConfigFile = "files/test/csvsource.properties";
		CsvSourceManager manager = null;
		
		try {
			manager = CsvSourceManager.readConfigurationFile(pathToTestConfigFile, true);
		} catch (IOException e) {
			Assert.fail("Test Configuration file (" + pathToTestConfigFile + ") not found.");
		}
		
		assertTrue("Name of Source should be 'csvsource' but is '" + manager.getNameOfSource() + "'",manager.getNameOfSource().equals("csvsource"));
		assertTrue("Description of Source should be 'CSV data published by someone somewhere', but is '" + manager.getDescriptionOfSource() +"'",manager.getDescriptionOfSource().equals("CSV data published by someone somewhere"));
		assertTrue("Reference File should be set as local file, but is set as " + manager.getLocalFile(), manager.getLocalFile().equals(true));
		assertTrue("Path to file should be 'files/test/basic.csv', but is '" + manager.getPathToFile() + "'", manager.getPathToFile().equals("files/test/basic.csv"));
		assertTrue("Path to local test file should be 'files/test/basic.csv', but is '" + manager.getPathToLocalTestFile() + "'", manager.getPathToLocalTestFile().equals("files/test/basic.csv"));
		assertTrue("Separator should be ',', but is '" + manager.getSeparator() + "'", manager.getSeparator() == ',');
		assertTrue("Quotechar should be '\"', but is '" + manager.getQuotechar() + "'", manager.getQuotechar() == '"');
		assertTrue("Line (to start) should be '0', but is '" + manager.getStartLine() + "'", manager.getStartLine() == 0 );
		assertTrue("Line (to stop) should be '10000', but is '" + manager.getReadMaxEntries() + "'", manager.getReadMaxEntries() == 10000 );
		
		
		//Namespaces
		String NS_HU = "http://www.hummel-universe.net/ids/";
		String NS_GEO = "http://www.w3.org/2003/01/geo/wgs84_pos#";
		String NS_EX = "http://example.org/uri/";
		String NS_XSD = "http://www.w3.org/2001/XMLSchema#";
		
		HashMap<String, String> expectedNamespaceDefinitions = new HashMap<String, String>();
		expectedNamespaceDefinitions.put("ex", NS_EX);
		expectedNamespaceDefinitions.put("hu", NS_HU);
		expectedNamespaceDefinitions.put("geo", NS_GEO);
		expectedNamespaceDefinitions.put("xsd", NS_XSD);
		
		/*System.out.println(expectedNamespaceDefinitions);
		System.out.println(manager.getNamespaceDefinitions());*/
		assertTrue("Received Namespacedefinitions do not equal expected ones.", manager.getNamespaceDefinitions().equals(expectedNamespaceDefinitions));
		
		//AdditionalStatements
		assertTrue("AttachAdditionalStatements should be true, but is '" + manager.getAdditionalStatements() + "'", manager.getAttachAdditionalStatements() == true );
		Model expectedAdditionalStatements = ModelFactory.createDefaultModel();
		expectedAdditionalStatements.setNsPrefixes(expectedNamespaceDefinitions);
		expectedAdditionalStatements.createResource(NS_EX + "hasObservationLatitude").addProperty(RDFS.subPropertyOf, NS_GEO + "lat");
		expectedAdditionalStatements.createResource(NS_EX + "hasObservationLongitude").addProperty(RDFS.subPropertyOf, NS_GEO + "long");

		/*expectedAdditionalStatements.write(System.out, "Turtle");
		manager.getAdditionalStatements().write(System.out, "Turtle");*/
		assertTrue("Received AdditionalStatemens do not equal expected ones.", manager.getAdditionalStatements().isIsomorphicWith(expectedAdditionalStatements));
	}
	
	/*
	 * Test if all "valid" and "active" configFiles do not throw an error when reading.
	 */
	@Test
	public void testAllConfigurationFiles() {
		File configFolder = new File("files/config");
		
	    //create a FilenameFilter and override its accept-method
	    FilenameFilter propertyFileFilter = new FilenameFilter() {

	      public boolean accept(File dir, String name) {
	        //if the file extension is .properties return true, else false
	        return name.endsWith(".properties");
	      }
	    };
		
		String[] propertyFileNames = configFolder.list(propertyFileFilter);
		
		for (String propFileName : propertyFileNames) {
			Properties config = new Properties();
			BufferedInputStream inputStream;
			
			//load file (depending on local/remote file)
			try {
				inputStream = new BufferedInputStream(new FileInputStream(configFolder + "/" + propFileName));
				config.load(inputStream);
				inputStream.close();
			} catch (IOException e) {
				Assert.fail("Cannot read config file '" + configFolder + "/" + propFileName + "'.\nStacktrace:\n" + e.toString());
			}
			
			// check only files that are tagged valid
			if (config.get("configFileValid").equals("true") && config.get("configFileActive").equals("true")) {
				try {
					CsvSourceManager.readConfigurationFile(configFolder + "/" + propFileName, true);
				} catch (Exception e) {
					Assert.fail("Config-File '" + propFileName + "' doesn't seem to be valid. Please mark as invalid or see for errors.\n Stacktrace:\n=========\n" + e.toString());
				}
			}
			
			//System.out.println(propFileName);
			
		}
	}


}
