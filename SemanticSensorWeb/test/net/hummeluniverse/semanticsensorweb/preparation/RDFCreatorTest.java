/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author Thomas Hummel
 * @since 19.04.2012
 *
 */
public class RDFCreatorTest {
	//fields

	//constructor

	//basic methods

	//other methods
	@Test
	public void testListToRdf(){
		
		/*
		 * Declare TestData
		 */
		String[] testValues1 = {"2012-01-01T00:00:00Z", "52.7", "-212667", "BAW7TG"};
		String[] testValues2 = {"2012-01-02T00:50:00Z", "62.7", "-222667", "BAW7TG"};
		
		//Namespaces
		String NS_HU = "http://www.hummel-universe.net/ids/";
		String NS_GEO = "http://www.w3.org/2003/01/geo/wgs84_pos#";
		String NS_EX = "http://example.org/uri/property/";
		String NS_XSD = "http://www.w3.org/2001/XMLSchema#";
		
		HashMap<String, String> readableNamespaces = new HashMap<String, String>();
		readableNamespaces.put("ex", NS_EX);
		readableNamespaces.put("geo", NS_GEO);
		readableNamespaces.put("hu", NS_HU);
		readableNamespaces.put("xsd", NS_XSD);
		
		// Description of the TestData		
		String[][] arrayDescriptions = {
				//{property, type, futherInfo}
				{NS_EX + "hasObservationTime", "Literal", "dateTime"}, //think of plain, @en, xsd datatypes, ...
				{NS_EX + "hasObservationLatitude", "Literal", "decimal"},
				{NS_EX + "hasObservationLongitude", "Literal", "double"},
				{NS_EX + "hasAircraftReference", "Resource", "AircraftReference"} //resource should be referenced with some uri
		};
		
		Boolean attachAdditionalStatements = true;
		Model additionalStatements = ModelFactory.createDefaultModel();
		additionalStatements.setNsPrefixes(readableNamespaces);
		additionalStatements.createResource(NS_EX + "hasObservationLatitude").addProperty(RDFS.subPropertyOf, NS_GEO + "lat");
		additionalStatements.createResource(NS_EX + "hasObservationLongitude").addProperty(RDFS.subPropertyOf, NS_GEO + "long");
				
		
		/*
		 * Prepare TestData for Processing
		 */
		ArrayList<String[]> inputList = new ArrayList<String[]>();
		Model rdfModel;
			
		
		inputList.add(testValues1);
		inputList.add(testValues2);
		
		

		rdfModel = RDFCreator.listToRdfModel(inputList, arrayDescriptions, readableNamespaces, additionalStatements, attachAdditionalStatements);
		
		assertNotNull("RDF-Model should not be null.",rdfModel);
		
		//For Debugging Purposes
		//additionalStatements.write(System.out, "RDF/XML-ABBREV");
		//additionalStatements.write(System.out, "Turtle");
		//rdfModel.write(System.out, "RDF/XML-ABBREV");
		//rdfModel.write(System.out, "Turtle");
		
		
		
		
		//TODO handle inputlists/arrays with empty parts
		
		//TODO handle inputlist without array description (read first line and use as property) - more can be specified with additionalStatements
	}
	
	@Test
	public void testStringToRDFLiteral(){
		String[][] testValues = {
				// dataValue, dataTye, resulting toString()
				{"value", "plain", "value"}, //test plain
				{"2.05", "decimal", "2.05^^http://www.w3.org/2001/XMLSchema#decimal"}, //test typed Literal
				{"5", "unsignedInt", "5^^http://www.w3.org/2001/XMLSchema#unsignedInt"}, //test typed Literal
				{"language", "@en", "language@en"}, // test language
				{"sprache", "@de", "sprache@de"}, // test language
				{"crappycrap", "lalelau", "crappycrap"} // test stupid datatype - should be handled as plain
		};
			
		String dataValue = null;
		String dataAdditionalInformation = null;
		String expectedResult = null;
		Literal actualResult = null;
		
		for (int i = 0; i < testValues.length; i++) {
			dataValue = testValues[i][0];
			dataAdditionalInformation = testValues[i][1];
			expectedResult = testValues[i][2];
			
		
			actualResult = RDFCreator.stringToRDFLiteral(dataValue, dataAdditionalInformation);
			
			
			assertTrue("Generated Literal (\"" + actualResult + "\") does not match expected Literal (\"" + expectedResult + "\")", expectedResult.equals(actualResult.toString()));
		}
	}
	
/*	@Ignore // FileWriter not allowed on GAE
	public void testRdfFileWriter(){
		Model rdfModel = null;
		String pathToSaveFile = "test/save.rdf";
		
		RDFCreator.rdfFileWriter(rdfModel, pathToSaveFile);
				
	}*/

}
