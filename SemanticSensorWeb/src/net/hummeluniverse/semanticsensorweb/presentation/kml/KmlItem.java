/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.util.ArrayList;

/**
 * @author Thomas Hummel
 * @since 08.06.2012
 *
 */
public abstract class KmlItem {
	/* fields */
	ArrayList<KmlItem> content = new ArrayList<KmlItem>();
	
	/* constructor */

	/* basic methods */

	/* other methods */
    public abstract StringBuffer writeKml();
    
	/**
	 * @param subFolderName
	 * @param subFolderId
	 * @param subFolderDescription
	 * @return
	 */
	public KmlFolder addKmlFolder(String subFolderName, String subFolderId,
			String subFolderDescription) {
		KmlFolder kmlFolder = new KmlFolder(subFolderName, subFolderId, subFolderDescription);
		this.content.add(kmlFolder);
		
		return kmlFolder;
	}
	
	/**
	 * @param subFolderName
	 * @param subFolderId
	 * @param subFolderDescription
	 * @return
	 */
	public Boolean add(KmlItem kmlItem) {
		this.content.add(kmlItem);
		
		return true;
	}
	
	/**
	 * @param title
	 * @param description
	 * @return
	 */
	public KmlPlacemark addKmlPlacemark(String title, String description) {
		KmlPlacemark kmlPlacemark = new KmlPlacemark(title, description);
		this.content.add(kmlPlacemark);
		
		return kmlPlacemark;
	}
	
	/**
	 * @param iconPath
	 * @return
	 */
	public KmlStyle addKmlStyle(String iconPath) {
		KmlStyle kmlStyle = new KmlStyle(iconPath);
		if(kmlStyle != null){
			this.content.add(kmlStyle);
		}
		
		return kmlStyle;
	}
}
