/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.util.Iterator;

/**
 * @author Thomas Hummel
 * @since 08.06.2012
 *
 */
public class KmlFolder extends KmlItem {

	/* fields */
	String title;
	String id;
	String description;

	/* constructor */
	KmlFolder(String subFolderName, String subFolderId, String subFolderDescription){
		if (subFolderName == null){subFolderName = "No Title";}
		this.title = subFolderName;
		if (subFolderId == null){subFolderId = "NOID";}
		this.id = subFolderId;
		if (subFolderDescription == null){subFolderDescription = "No description yet";}
		this.description = subFolderDescription;
	}

	/* basic methods */

	/* other methods */
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#writeKml()
	 */
	@Override
	public StringBuffer writeKml() {
		StringBuffer folder = new StringBuffer();
		folder.append("" +
				"  <Folder id=\"" + this.id + "\">\n" + 
				"    <name>" + this.title + "</name>\n" +
				"    <open>1</open>\n" +
				"    <description>\n" +
				this.description + 
				"    </description>\n");
		
		if (!this.content.isEmpty()) {
			Iterator<KmlItem> iter = this.content.iterator();
			while (iter.hasNext()){
				folder.append(iter.next().writeKml());
			}
		}
		
		folder.append("  </Folder>\n");

		return folder;
	}

}
