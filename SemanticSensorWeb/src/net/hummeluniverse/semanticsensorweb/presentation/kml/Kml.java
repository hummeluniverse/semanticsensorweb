/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.io.PrintWriter;
import java.util.Iterator;

import org.apache.log4j.Logger;

/**
 * @author Thomas Hummel
 * @since 08.06.2012
 *
 */
public class Kml extends KmlItem {
	private final static Logger LOG = Logger.getLogger(Kml.class.getName());
	
	
	/* fields */
	String xmlDeclaration = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>";
	String kmlStartTag = "<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:xal=\"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0\">";
	String kmlEndTag = "</kml>";
	

	/* constructor */
	
	/* basic methods */

	/* other methods */
	/** 
	 * 
	 **/
	public StringBuffer writeKml() {
		StringBuffer basicStructure = new StringBuffer();
		basicStructure.append("" +
				"<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n" +
				"<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:xal=\"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0\">\n" +
				"<Document>\n");
		
		if (!this.content.isEmpty()) {
			Iterator<KmlItem> iter = this.content.iterator();
			while (iter.hasNext()){
				basicStructure.append(iter.next().writeKml());
			}
		}
		
		basicStructure.append("</Document>\n</kml>");
		LOG.debug("Kml written");
		return basicStructure;
	}

	/**
	 * @param writer
	 */
	public void write(PrintWriter writer) {
		writer.write(this.writeKml().toString());
		writer.close();
	}
	
}
