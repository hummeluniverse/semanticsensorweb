/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import net.hummeluniverse.semanticsensorweb.helper.DataSource;
import net.hummeluniverse.semanticsensorweb.helper.TimeMeasurement;

import com.hp.hpl.jena.rdf.model.LiteralRequiredException;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.shared.InvalidPropertyURIException;
import com.hp.hpl.jena.vocabulary.RDF;


/**
 * @author Thomas Hummel
 * @since 26.05.2012
 *
 */
public class KmlCreator {
	private final static Logger LOG = Logger.getLogger(KmlCreator.class.getName());
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */

	/**
	 * @param config 
	 * @param basicRdfModel
	 * @param additionalRdfModels
	 * @return 
	 */	
	public static Kml rdfToKml (KmlConfigManager config, Model basicRdfModel, ArrayList<Model> additionalRdfModels) throws IOException {
		Kml kml = new Kml();
		Model rdfModel = basicRdfModel;
		//Model[] additionalRdfModels;
		ResIterator relevantResources;
		long startTime;
		int numberOfPlacemarks = 0;


		/* Add specific IconStyle if defined */
		String iconPath = config.getIconPath();
		System.out.println("Iconpath = " + iconPath);
		if (iconPath != null){
			kml.addKmlStyle(iconPath);
		}

		/* Data for each Folder with data */
		String subFolderName = config.getFolderName();
		String subFolderId = config.getFolderId();
		String subFolderDescription = config.getFolderDescription();
		KmlFolder subFolder = kml.addKmlFolder(subFolderName, subFolderId, subFolderDescription);
		
		String urlToSource = config.getUrlToSource();
		String urlToTestSource = config.getUrlToTestSource();


		/* Filer relevant resources */
		startTime = TimeMeasurement.printStartMessage("Starting to filter relevant Resources", LOG);
		String relevantProperty = config.getRelevantProperty();

		try {
			relevantResources = rdfModel.listSubjectsWithProperty(RDF.type, rdfModel.createResource(relevantProperty));
		}
		catch (NullPointerException e) {
			throw new IllegalArgumentException("No all relevant properties set in configfile (" + config.getPathToPropertiesFile() + ")? Please check!\n - relevantProperties missing!");
		}
		TimeMeasurement.printStopMessage("Filtering of relevant Resources finished", startTime, LOG);


		/* Produce KML Placemarks */
		startTime = TimeMeasurement.printStartMessage("Starting to produce KML placemarks", LOG);

		String placementCreationMethod = config.getPlacementCreationMethod();

		if (placementCreationMethod.equals("Standard")) {

			Property titleProperty = ResourceFactory.createProperty(config.getTitleProperty());
			Property observationTimeProperty = ResourceFactory.createProperty(config.getObservationTimeProperty());
			Property latitudeProperty = ResourceFactory.createProperty(config.getLatitudeProperty());
			Property longitudeProperty = ResourceFactory.createProperty(config.getLongitudeProperty());
			Property altitudeProperty = ResourceFactory.createProperty(config.getAltitudeProperty());

			//Property measurementResources = ResourceFactory.createProperty("http://projects.hummel-universe.net/semanticsensorweb/property/hasTemperature_c");
			String[][] measurementValues = config.getMeasurementProperties();
			Property[] measurementProperties = null;


			if (measurementValues != null){
				measurementProperties = new Property[measurementValues.length];
				for (int i = 0; i < measurementValues.length; i++) {
					measurementProperties[i] = ResourceFactory.createProperty(measurementValues[i][1]);
				}
			}


			while(relevantResources.hasNext()){

				Resource measurementResource = relevantResources.nextResource();

				if(measurementResource.isResource()) {

					String title = "";
					String description = "";
					//String temperature = "";
					String observationTime = "";
					String longitude = null;
					String latitude = null;
					String altitudeMode = config.getAltitudeMode();
					String altitudeUnit = config.getAltitudeUnit();
					String altitude = null;
					String urlToMeasurementResource;

					urlToMeasurementResource = measurementResource.toString();

					try{
						if (titleProperty != null) {
							title = measurementResource.getProperty(titleProperty).getString();
						}
					} catch (NullPointerException e){};








					try {
						latitude = measurementResource.getProperty(latitudeProperty).getString();
					} catch (NullPointerException e){
						latitude = null;
					}

					try {
						longitude = measurementResource.getProperty(longitudeProperty).getString();
					} catch (NullPointerException e){
						latitude = null;
					}

					try {
						altitude = measurementResource.getProperty(altitudeProperty).getString();
					} catch (NullPointerException e){
						altitude = null;
					}

					description = description + "<hr/><h4>Basic Facts:</h4>\n<ul>\n";
					description = description + "<li><b>Lat/Long:</b> " + latitude + "/" + longitude + "</li>\n";
					if (altitude != null && altitudeMode != null && altitudeUnit != null) {
						description = description + "<li><b>Alt:</b> " + altitude + " (" + altitudeUnit + " - " + altitudeMode + ")</li>\n";
					}
					description = description + "</ul>\n";



					description = description + "<hr/><h4>Measurements:</h4>\n<ul>\n";

					if (measurementValues != null){
						for (int i=0; i < measurementValues.length; i++) {
							String measurementDescription = measurementValues[i][0];
							String measurementValue = "";
							try {
								measurementValue = measurementResource.getProperty(measurementProperties[i]).getString();
							}
							catch (NullPointerException e) {
								//TODO write to Logfile
							} catch (InvalidPropertyURIException f){
								//TODO write to logfile
							}

							if (measurementDescription != "" && measurementValue != "") {
								description = description + "<li><b>" + measurementDescription + ":</b> " + measurementValue + "</li>\n";
							}

						}
					}

					description = description + "</ul>\n";

					try {
						observationTime = measurementResource.getProperty(observationTimeProperty).getString();
						if (observationTime != "") {
							description = description + "<br/>\n<p align=\"right\"><small><b>Observation Date:</b><br/>" + observationTime + "</small></p>";
						}
					} catch (NullPointerException e){

					}

					description = description + "<hr/><h4>Sources:</h4>\n<ul>\n";
					URL url;
					url = new URL (urlToMeasurementResource);
					String urlHost = url.getHost().toString();
					description = description + "<li><b>Measurement:</b> <a href=\"" + urlToMeasurementResource + "\"><nobr>" + urlHost +"</nobr></a></li>\n";
					    
					/*					String urlToAdditionalResource = null;
					if (urlToAdditionalResource != null && urlToAdditionalResource != "") {
						url = new URL (urlToAdditionalResource);
						urlHost = url.getHost().toString();
						description = description + "<li><b>Additional:</b> <a href=\"" + urlToAdditionalResource + "\"><nobr>" + urlHost +"</nobr></a></li>\n";
					}*/
					description = description + "\n</ul><hr/>";
					description = description + "<b>Request complete source RDF:</b> <a href=\"" + urlToSource + "\"><nobr>Live</nobr></a> - <a href=\"http://projects.hummel-universe.net/semanticsensorweb/" + urlToTestSource + "\"><nobr>Demo</nobr></a>\n";	  

					//TODO urlToTestSource is bad - relative and absolute links!
					
					/*
					 * Set values to KML-Object
					 */
					Boolean useAltitude = false;
					if (altitude != null && altitudeMode != null && altitudeUnit != null) {
						useAltitude = true;
					}

					if (latitude != null && longitude != null){
						if (useAltitude){
							subFolder.addKmlPlacemark(title, description).addKmlPoint(longitude, latitude, altitudeMode, altitude, altitudeUnit);
						} else {
							subFolder.addKmlPlacemark(title, description).addKmlPoint(longitude, latitude);
						}
					}

					numberOfPlacemarks++;
				}
			}

		} else if (placementCreationMethod.equals("Metar_and_Dbpedia")) {

			Property titleProperty = ResourceFactory.createProperty("http://projects.hummel-universe.net/semanticsensorweb/property/hasStationID");
			Property temperatureProperty = ResourceFactory.createProperty("http://projects.hummel-universe.net/semanticsensorweb/property/hasTemperature_c");
			Property observationTimeProperty = ResourceFactory.createProperty("http://projects.hummel-universe.net/semanticsensorweb/property/hasObservationTime");
			Property latitudeProperty = ResourceFactory.createProperty("http://www.w3.org/2003/01/geo/wgs84_pos#lat");
			Property longitudeProperty = ResourceFactory.createProperty("http://www.w3.org/2003/01/geo/wgs84_pos#long");

			Property icaoAdditionalProperty = ResourceFactory.createProperty("http://dbpedia.org/ontology/icaoLocationIdentifier");
			Property airportNameAdditionalProperty = ResourceFactory.createProperty("http://dbpedia.org/property/name");
			Property airportLocalNameAdditionalProperty = ResourceFactory.createProperty("http://dbpedia.org/property/nativename");

			while(relevantResources.hasNext()){

				Resource measurementResource = relevantResources.nextResource();

				if(measurementResource.isResource()) {

					String title = "";
					String description = "";
					String temperature = "";
					String observationTime = "";
					String longitude = "0";
					String latitude = "0";
					//String altitude = null;
					//String altitudeMode = null;
					//String altitudeUnit = null;
					String urlToMeasurementResource;

					urlToMeasurementResource = measurementResource.toString();

					String icao = "";

					String urlToAdditionalResource = "";
					String airportName = "";
					String airportNativeName = "";


					try{icao = measurementResource.getProperty(titleProperty).getString();
					} catch (NullPointerException e){};
					try {temperature = measurementResource.getProperty(temperatureProperty).getString();
					} catch (NullPointerException e){/*System.out.println(temperature);*/};
					try {observationTime = measurementResource.getProperty(observationTimeProperty).getString();
					} catch (NullPointerException e){/*System.out.println(temperature);*/};
					try {latitude = measurementResource.getProperty(latitudeProperty).getString();
					} catch (NullPointerException e){/*System.out.println(latitude);*/};
					try {longitude = measurementResource.getProperty(longitudeProperty).getString();
					} catch (NullPointerException e){/*System.out.println(longitude);*/};


					if (additionalRdfModels != null && !additionalRdfModels.isEmpty() && icao != ""){
						//StmtIterator relevantAdditionalResources = additionalRdfModels[0].listStatements(null, ResourceFactory.createProperty("http://dbpedia.org/ontology/icaoLocationIdentifier"), icao);
						ResIterator relevantAdditionalResources = additionalRdfModels.get(0).listSubjectsWithProperty(icaoAdditionalProperty, icao, "en");

						if (relevantAdditionalResources.hasNext()){
							Resource resource = relevantAdditionalResources.next();

							urlToAdditionalResource = resource.toString();
							try{
								airportName = resource.getProperty(airportNameAdditionalProperty).getString();
							} catch (NullPointerException e){} catch (LiteralRequiredException l){}
							try{
								airportNativeName = resource.getProperty(airportLocalNameAdditionalProperty).getString();
							} catch (NullPointerException e){} catch (LiteralRequiredException l){}
						}
					}

					/*
					 * Prepare values for KML-Object
					 */

					//title
					if (airportName != "") {
						title = airportName;
						if (icao != "") {
							title = title + " (" + icao + ")";
						}
					} else if (airportNativeName != "") {
						title = airportName;
						if (icao != "") {
							title = title + " (" + icao + ")";
						}
					} else if (icao != "") {
						title = icao;
					} else {
						title = "NoTitle";
					}

					
					
					//description
					description = description + "<hr/><h4>Basic Facts:</h4>\n<ul>\n";
					if (airportName != "") {
						description = description + "<li><b>Name of Airport:</b> " + airportName + "</li>\n";
					}
					if (airportNativeName != "") {
						description = description + "<li><b>Native name of Airport:</b> " + airportNativeName + "</li>\n";
					}
					if (icao != "") {
						description = description + "<li><b><a href=\"http://en.wikipedia.org/wiki/International_Civil_Aviation_Organization_airport_code\">ICAO</a>:</b> " + icao + "</li>\n";
					}
					description = description + "<li><b>Lat/Long:</b> " + latitude + "/" + longitude + "</li>\n";
					description = description + "</ul>\n";

					
					
					
					description = description + "<hr/><h4>Measurements:</h4>\n<ul>\n";
					if (temperature != "") {
						description = description + "<li><b>Temperature:</b> " + temperature + "�C</li>\n";
					}
					description = description + "</ul><br/>\n";
					if (observationTime != "") {description = description + "<br/>\n<p align=\"right\"><small><b>Observation Date:</b><br/>" + observationTime + "</small></p>";
					}



					description = description + "<hr/><h4>Sources:</h4>\n<ul>\n";

					URL url;
					url = new URL (urlToMeasurementResource);
					String urlHost = url.getHost().toString();
					description = description + "<li><b>Measurement:</b> <a href=\"" + urlToMeasurementResource + "\"><nobr>" + urlHost +"</nobr></a></li>\n";	        		
					if (urlToAdditionalResource != "") {
						url = new URL (urlToAdditionalResource);
						urlHost = url.getHost().toString();
						description = description + "<li><b>Additional:</b> <a href=\"" + urlToAdditionalResource + "\"><nobr>" + urlHost +"</nobr></a></li>\n";
					}
					description = description + "</ul><hr/>\n";
					description = description + "<b>Request complete source RDF:</b> <a href=\"" + urlToSource + "\"><nobr>Live</nobr></a> - <a href=\"http://projects.hummel-universe.net/semanticsensorweb/" + urlToTestSource + "\"><nobr>Demo</nobr></a>\n";
					//TODO urlToTestSource is bad
					//escaped automatically
					//description = StringEscapeUtils.escapeHtml4(description);
					//description = "<![CDATA[" + description + "]]>";

					/*
					 * Set values to KML-Object
					 */

					//System.out.println(measurementResource);
					subFolder.addKmlPlacemark(title, description).addKmlPoint(longitude, latitude);//altitudeMode, altitude

					numberOfPlacemarks++;
				}
			}

		} else {

		}

		TimeMeasurement.printStopMessage("Producing KML placemarks finished (" + numberOfPlacemarks + " placemarks in total)", startTime, LOG);
		//kml.setFeature(placemark);         // <-- placemark is registered at kml ownership.
		//kml.marshal(System.out);           // <-- Print the KML structure to the console.
		return kml;
		//kml.marshal(new File("HelloKml.kml"));
	}

	/**
	 * @param config
	 * @return
	 */
	public static ArrayList<Model> loadAdditionalDatasets(KmlConfigManager config) {
		long startTime = TimeMeasurement.printStartMessage("Starting to load additional datasets", LOG);
		ArrayList<Model> additionalDatasetsRDF = new ArrayList<Model>();
		int numberOfAdditionalDatasets = 0;

		String[] additionalDatasets = config.getAdditionalDatasets();

		if (additionalDatasets != null) {
			for (String additionalDataset : additionalDatasets) {
				if (additionalDataset != null){
					KmlAdditionalDatasetManager properties = null;
					LOG.debug("Additonal Dataset: " + additionalDataset);
					try {
						properties = new KmlAdditionalDatasetManager(additionalDataset);
					} catch (IOException e) {
						// TODO write to logfile - path to additionaldataset is wrong
						LOG.warn("Additonal Dataset ('" + additionalDataset + "') could not be found. Wrong path?");
					}

					if (properties != null){
						String additionalDataSetType = properties.getType(); //Sparql, File
						LOG.debug("AdditionalDataSetType = " + additionalDataSetType);
						if(additionalDataSetType != null){
							if (additionalDataSetType.equalsIgnoreCase("Sparql")) {

								String additionalDataSetUrl = properties.getUrl(); // url to file or to sparql endpoint
								String sparqlQuery = properties.getSparqlQuery();
								
								LOG.debug("Dataset-Url:\n" + additionalDataSetUrl);
								LOG.debug("Sparql-Query:\n" + sparqlQuery);

								if (additionalDataSetUrl != null && sparqlQuery != null){
									Model additionalRdfModel = DataSource.sparqlQuery(additionalDataSetUrl, sparqlQuery);
									additionalDatasetsRDF.add(additionalRdfModel);
									numberOfAdditionalDatasets++;
								} else {
									//TODO write to logfile - dataseturl or sparqlquery empty
								}


							} else if (additionalDataSetType.equalsIgnoreCase("File")) { //File

								LOG.debug("Additional File dataset loaded");
								String additionalDataSetUrl = properties.getUrl(); // url to file or to sparql endpoint
								String additionalDataSetFormat = properties.getFormat(); // result format RDF/XML or Turtle ....

								LOG.debug("Dataset-Url:\n" + additionalDataSetUrl);
								LOG.debug("Dataset-Format:\n" + additionalDataSetFormat);
								
								if (additionalDataSetUrl != null && additionalDataSetFormat != null){
									Model additionalRdfModel = DataSource.readRdfFile(additionalDataSetUrl, additionalDataSetFormat);
									additionalDatasetsRDF.add(additionalRdfModel);
									numberOfAdditionalDatasets++;
								} else {
									//TODO write to logfile - dataseturl or datasetformat empty
								}


							} else {
								//TODO write to logfile - no correct type set
							}
						} else {
							//TODO write to logfile - no type set at all
						}
					}
				}


			}

		}

		TimeMeasurement.printStopMessage("Loading additional datasets finished. " + numberOfAdditionalDatasets + " datasets loaded.", startTime, LOG);
		return additionalDatasetsRDF;

	}
}
