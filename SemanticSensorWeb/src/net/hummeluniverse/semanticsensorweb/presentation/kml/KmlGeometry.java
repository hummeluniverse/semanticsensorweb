/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.text.DecimalFormat;

/**
 * @author Thomas Hummel
 * @since 08.06.2012
 *
 */
public class KmlGeometry extends KmlItem {

	/* fields */
	String geometryType = "Point"; //Point or more
	String extrude = "0";
	String altitudeMode = "clampToGround";
	String longitude;
	String latitude;
	String altitude;

	/* constructor */
	
	/**
	 * @param geometryType
	 * @param longitude
	 * @param latitude
	 * @param altitudeMode
	 * @param altitude
	 */
	public KmlGeometry(String geometryType, String longitude, String latitude,
			String altitudeMode, String altitude, String altitudeUnit) {
		this.geometryType = geometryType;
		this.longitude = longitude;
		this.latitude = latitude;
		
		Boolean addAltitude = true;
		String altitudeModeNew = null;
		String strAltitudeMeter = null;
		
		if (altitudeMode != null && altitude != null && altitudeUnit != null){
			
			
			if (altitudeUnit.equals("m") || altitudeUnit.equalsIgnoreCase("meter") || altitudeUnit.equalsIgnoreCase("metre") || altitudeUnit.equalsIgnoreCase("meters") || altitudeUnit.equalsIgnoreCase("metres")) {
					this.altitude = altitude;
			}
			else if (altitudeUnit.equals ("ft")|| altitudeUnit.equalsIgnoreCase("foot") || altitudeUnit.equalsIgnoreCase("feet")){
				Double oneFootInMeter = 0.3048;
				altitude = altitude.replace(",", ".");
				Double altitudeDouble = Double.parseDouble(altitude);
				
				Double altitudeMeter = altitudeDouble * oneFootInMeter;
				DecimalFormat f = new DecimalFormat("#0.####"); 
				
				strAltitudeMeter = f.format(altitudeMeter).replace(",", ".");
			} else {
				addAltitude = false; // no value for altitude
				strAltitudeMeter = "0"; // zero clampToGround, should not be necessary
			}
			
			if (altitudeMode.equals("absolute") || altitudeMode.equals("absolute") || altitudeMode.equals("absolute") || altitudeMode.equals("absolute")) {
				altitudeModeNew = altitudeMode;
			} else {
				addAltitude = false; // no value for altitudeMode, maybe absolute?
				altitudeMode = "clampToGround"; // not necessary - should already be set
			}
		}		
		
		if(addAltitude){
			this.altitude = strAltitudeMeter;
			this.altitudeMode = altitudeModeNew;
			this.extrude = "1";
		}
		
	}

	/* basic methods */

	/* other methods */

	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#writeKml()
	 */
	@Override
	public StringBuffer writeKml() {
		if (geometryType != null) {
			if (geometryType.equals("Point")){
				return this.writeKmlPoint();
			} else {
				return new StringBuffer();
			}
		} else {
			return new StringBuffer();
		}
	}
	
	/**
	 * 
	 **/
	private StringBuffer writeKmlPoint() {
		StringBuffer point = new StringBuffer();
		String coordinates = null;
		if (this.longitude != null || this.latitude != null){
			if(this.altitude != null && this.altitudeMode != null){
				coordinates = this.longitude + "," + this.latitude + "," + this.altitude;
			} else {
				coordinates = this.longitude + "," + this.latitude;
				this.altitudeMode = "clampToGround";
				this.extrude = "0";
			}
		}
		
			
		if (coordinates != null){
			point.append("" +
					"            <Point>\n" +
					"                <extrude>" + this.extrude + "</extrude>\n" +
					"                <altitudeMode>" + this.altitudeMode + "</altitudeMode>\n" +
					"                <coordinates>" + coordinates + "</coordinates>\n" +
					"            </Point>\n");
		}

		return point;
	}

}
