/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.io.IOException;

//import org.apache.log4j.Logger;

import net.hummeluniverse.semanticsensorweb.helper.MainConfigManager;


/**
 * @author Thomas Hummel
 * @since 09.06.2012
 *
 */
public class KmlAdditionalDatasetManager extends MainConfigManager {
	//private final static Logger LOG = Logger.getLogger(KmlAdditionalDatasetManager.class.getName());

	/* constructor */
	public KmlAdditionalDatasetManager(String configName) throws IOException{
		super(configName, "kml/config/additional_datasets/" + configName);
	}

	/* basic methods */
	
	/**
	 * @return
	 */
	public String getType() {
		return readConfigParam("type", null);
	}
	
	/**
	 * @return
	 */
	public String getUrl() {
		return readConfigParam("url", null);
	}
	
	/**
	 * @return
	 */
	public String getFormat() {
		return readConfigParam("format", null);
	}
	
	/**
	 * @return
	 */
	public String getSparqlQuery() {
		return readConfigParam("sparqlQuery", null);
	}

	/* other methods */

}
