/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

/**
 * @author Thomas Hummel
 * @since 11.06.2012
 *
 */
public class KmlStyle extends KmlItem {


	/* fields */
	String iconPath;

	/* constructor */
	/**
	 * @param description
	 */
	public KmlStyle(String iconPath) {
		if (iconPath != null){
			this.iconPath = iconPath;
		}
		
		
		//RuntimeChecker.getHeapStats();
	}

	/* basic methods */

	/* other methods */
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#writeKml()
	 */
	@Override
	public StringBuffer writeKml() {
		StringBuffer result = new StringBuffer();
		if (!this.iconPath.isEmpty()) {
			result.append("" +
					"<Style id=\"My_Style\">\n" +
					" <IconStyle>\n" +
					"    <Icon>\n" +
					"	  <href>" + this.iconPath + "</href>\n" +
					"    </Icon>\n" +
					"   <scale>2.2</scale>\n" +
					" </IconStyle>\n" +
					"</Style>\n");
		}
		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public KmlFolder addKmlFolder(String subFolderName, String subFolderId,
			String subFolderDescription) {
		throw new IllegalArgumentException("KmlStyle cannot contain futher elements!");
	}
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public KmlPlacemark addKmlPlacemark(String title, String description) {
		throw new IllegalArgumentException("KmlStyle cannot contain futher elements!");
	}
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public KmlStyle addKmlStyle(String iconPath) {
		throw new IllegalArgumentException("KmlStyle cannot contain futher elements!");
	}

}
