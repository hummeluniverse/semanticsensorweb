/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;


/**
 * @author Thomas Hummel
 * @since 08.06.2012
 *
 */
public class KmlPlacemark extends KmlItem {


	/* fields */
	String title;
	String description;
	KmlGeometry geometry;


	/* constructor */

	/**
	 * @param title
	 * @param description
	 */
	public KmlPlacemark(String title, String description) {
		if (title == null || title == ""){
			title = "No Title";}
		else {
			this.title = title;
		}
		
		if (description == null){
			description = "No Description";}
		else {
			this.description = description;
		}
		
		
		//RuntimeChecker.getHeapStats();
	}

	
	
	/* basic methods */

	/* other methods */
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#writeKml()
	 */
	@Override
	public StringBuffer writeKml() {
		StringBuffer placemark = new StringBuffer();

		placemark.append("" +
				"        <Placemark>\n" +
				"            <name>" + this.title + "</name>\n" +
				"            <visibility>0</visibility>\n" +
				"            <open>0</open>\n" +
				"            <description><![CDATA[" + this.description + "]]>\n" +
				"		     </description>\n" +
				"			 <styleUrl>#My_Style</styleUrl>\n");
		
		if (this.geometry != null){
			placemark.append(this.geometry.writeKml());
		}
				
		placemark.append("        </Placemark>\n");

		return placemark;
	}

	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public Boolean add(KmlItem kmlItem) {
		throw new IllegalArgumentException("KmlPlacemarks can only contain geometry elements!");
	}

	
	public Boolean add(KmlGeometry kmlGeometry) {
		
		this.geometry.add(kmlGeometry);
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public KmlFolder addKmlFolder(String subFolderName, String subFolderId,
			String subFolderDescription) {
		throw new IllegalArgumentException("KmlPlacemarks can only contain geometry elements!");
	}
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public KmlPlacemark addKmlPlacemark(String title, String description) {
		throw new IllegalArgumentException("KmlPlacemarks can only contain geometry elements!");
	}
	
	/* (non-Javadoc)
	 * @see net.hummeluniverse.semanticsensorweb.presentation.kml.KmlItem#add()
	 */
	@Override
	public KmlStyle addKmlStyle(String iconPath) {
		throw new IllegalArgumentException("KmlPlacemarks can only contain geometry elements!");
	}



	/**
	 * @param longitude
	 * @param latitude
	 * @param altitudeMode
	 * @param altitude
	 * @return
	 */
	public KmlGeometry addKmlPoint(String longitude, String latitude,
			String altitudeMode, String altitude, String altitudeUnit) {
		
		KmlGeometry kmlGeometry = new KmlGeometry("Point", longitude, latitude, altitudeMode, altitude, altitudeUnit);
		this.geometry = kmlGeometry;
		
		return kmlGeometry;
	}
	
	/**
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	public KmlGeometry addKmlPoint(String longitude, String latitude) {
		return addKmlPoint(longitude, latitude, null, null, null);
	}
}
