/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.io.IOException;

import net.hummeluniverse.semanticsensorweb.helper.MainConfigManager;


/**
 * @author Thomas Hummel
 * @since 29.05.2012
 *
 */
public class KmlConfigManager extends MainConfigManager{


	/* fields */
	//superclass
	

	/* constructor */
	public KmlConfigManager(String configName) throws IOException {
		super(configName, "kml/config/" + configName + ".properties");
		
	}
	
	/* basic methods */
	/**
	 * @return
	 */
	public String getUrlToSource() {

		String urlToSource = this.propertiesFile.getProperty("urlToSource");

		return urlToSource;
	}
	
	/**
	 * @return
	 */
	public String getPathToPropertiesFile() {

		return this.propertiesFilePath;
	}

	/**
	 * @return
	 */
	public String getUrlToTestSource() {

        return this.propertiesFile.getProperty("urlToTestSource");
	}

	/**
	 * @return
	 */
	public String getSourceFormat() {

		return readConfigParam("sourceFormat", "RDF/XML");
	}


	/**
	 * @return
	 */
	public String getTestSourceFormat() {

		return readConfigParam("testSourceFormat", "RDF/XML");
	}
	

	/**
	 * @return
	 */
	public String getFolderId() {

		return readConfigParam("folderId", "NoFolderId");
	}

	/**
	 * @return
	 */
	public String getFolderName() {

		return readConfigParam("folderName", "No Folder Title");
	}

	
	/**
	 * @return
	 */
	public String getFolderDescription() {

		return readConfigParam("folderDescription", "No Folder Description");
	}
	
	
	/**
	 * @return
	 */
	public String getRelevantProperty() {

		return readConfigParam("relevantPropertyOfType", "http://example.org/ids/measurement");
	}
	
	/**
	 * @return
	 */
	public String getPlacementCreationMethod() {

		return readConfigParam("placementCreationMethod", "Standard");
	}

	
	/**
	 * @return
	 */
	public String getTitleProperty() {

		return readConfigParam("titleProperty", "http://www.w3.org/2000/01/rdf-schema#label");
	}



	/**
	 * @return
	 */
	public String[][] getMeasurementProperties() {
		String measurementProperties = this.propertiesFile.getProperty("measurementProperties");
		String[][] measurementPropertiesArr = null;
		
		if (measurementProperties != null && measurementProperties != "" && measurementProperties != " "){
		//get array split up by the semicolon
		String[] measurementPropertiesString = measurementProperties.split(";");
		//create the two dimensional array with correct size
		measurementPropertiesArr =  new String[measurementPropertiesString.length][2];
		//combine the arrays split by semicolon and comma 
		  for(int i = 0;i < measurementPropertiesArr.length;i++) {
		      measurementPropertiesArr[i] = measurementPropertiesString[i].split(", ");
		  }
		}
		
		return measurementPropertiesArr;
	}
	
	/**
	 * @return Names of the additional datasets as defined in the properties file with the param "additionalDatasets"
	 */
	public String[] getAdditionalDatasets() {
		String additionalDatasetsInput = this.propertiesFile.getProperty("additionalDatasets");
		String[] additionalDatasetsArr = null;

		if (additionalDatasetsInput != null && additionalDatasetsInput != "" && additionalDatasetsInput != " "){
			//get array split up by the semicolon
			additionalDatasetsArr = additionalDatasetsInput.split(";");

			//remove blank spaces
			for (String string : additionalDatasetsArr) {
				while(string.startsWith(" ")){
					string = string.substring(1);
				}
			}
		}

		return additionalDatasetsArr;
	}




	/**
	 * @return String of the observation time property url as defined in the properties file with the param "observationTimeProperty" (default: "http://projects.hummel-universe.net/property/hasObservationTime")
	 */
	public String getObservationTimeProperty() {

		return readConfigParam("observationTimeProperty", "http://projects.hummel-universe.net/property/hasObservationTime");
	}



	/**
	 * @return String of latitude property url as defined in the properties file with the param "latitudeProperty" (default: "http://www.w3.org/2003/01/geo/wgs84_pos#lat")
	 */
	public String getLatitudeProperty() {

		return readConfigParam("latitudeProperty", "http://www.w3.org/2003/01/geo/wgs84_pos#lat");
	}



	/**
	 * @return String of longitude property as defined in the properties file with the param "longitudeProperty" (default: http://www.w3.org/2003/01/geo/wgs84_pos#long)
	 */
	public String getLongitudeProperty() {
		
		return readConfigParam("longitudeProperty", "http://www.w3.org/2003/01/geo/wgs84_pos#long");
	}
	
	/**
	 * @return
	 */
	public String getAltitudeMode() {
		
		return readConfigParam("altitudeMode", null);
	}
	
	/**
	 * @return
	 */
	public String getAltitudeProperty() {
		
		return readConfigParam("altitudeProperty", "http://projects.hummel-universe.net/semanticsensorweb/property/hasAltitude_ft_msl");
	}
	
	/**
	 * @return
	 */
	public String getAltitudeUnit() {
	
		return readConfigParam("altitudeUnit", null);
	}
	
	/**
	 * @return
	 */
	public String getIconPath() {
		
		return readConfigParam("iconPath", null);
	}


	/* other methods */

	/**
	 * This method is only a String-replacement method
	 * @param originalString String, where a replacement should be done
	 * @param newServerAddress String for the string.replaceAll(regex, replacement) regex method
	 * @param actualServerAddress new String for replacement
	 * @return
	 */
	public static String replaceServerAdress(String originalString, String oldServerAddress, String newServerAddress) {
		String result = originalString;
		result = result.replaceAll(oldServerAddress, newServerAddress); 
		return result;
	}
}
