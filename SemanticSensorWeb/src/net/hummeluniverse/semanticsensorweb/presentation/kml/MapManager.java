/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.presentation.kml;

import java.io.IOException;

import net.hummeluniverse.semanticsensorweb.helper.MainConfigManager;


/**
 * @author Thomas Hummel
 * @since 09.06.2012
 *
 */
public class MapManager extends MainConfigManager {
	/* fields */

	/* constructor */
	public MapManager(String configName) throws IOException {
		super(configName, "kml/config/maps/" + configName + ".properties");
	}

	/* basic methods */
	/**
	 * @return String of the map's start location as defined in the properties file with the param "mapStartLatLong" (default: "50.037524, 8.560949" - Frankfurt Airport, Germany)
	 */
	public String getMapStartLatLong() {
		return readConfigParam("mapStartLatLong", "50.037524, 8.560949");
	}
	
	/**
	 * @return String of the map's start zoom as defined in the properties file with the param "mapStartZoom" (default: "6" - with Frankfurt Airport as center it covers more or less central Europe)
	 */
	public String getMapStartZoom() {
		return readConfigParam("mapStartZoom", "6");
	}
	
	/**
	 * @return String of the filePath to the KML-Overlay as defined in the properties file with the param "mapKmlLink" (default: null)
	 */
	public String getMapKmlLink() {
		return readConfigParam("mapKmlLink", null);
	}

	/* other methods */
	/**
	 * @return This method replaces _LATLONG_ _ZOOM_ and _PATHTOKML_ of a given String (probably html file with the map) with the values defined in the properties file.
	 */
	public String replaceHtmlMap(String htmlMap) {
		String result = htmlMap;

		String latlong = this.getMapStartLatLong();
		result = result.replaceAll("_LATLONG_", latlong);

		String startZoom = this.getMapStartZoom();
		result = result.replaceAll("_ZOOM_", startZoom);
		
		String kmlLink = this.getMapKmlLink();
		if (kmlLink != null){
			result = result.replaceAll("_PATHTOKML_", kmlLink);
		} else {
			throw new IllegalArgumentException("mapKmlLink should not be null. Please make sure that it is correctly set in the properties file.");
		}
		
		
		return result;
	}

}
