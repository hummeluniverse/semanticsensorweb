package net.hummeluniverse.semanticsensorweb.provision;

import java.io.IOException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class PropertyInformationServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		/*resp.setContentType("text/plain");
		resp.getWriter().println("In a future version of the SemanticSensorWeb-Service, you will be able to acces more detailled information about this property here. For now only this text shows its presence.");
*/
	/*		List<String> mimeTypesSupported = Arrays.asList(StringUtils.split(
    "application/xbel+xml,text/xml", ','));
String bestMatch = MIMEParse.bestMatch(mimeTypesSupported, "text/*;q=0.5,;q=0.1");*/
	
	
		String pathToUriBase = "http://projects.hummel-universe.net/semanticsensorweb";
		String pathToUriProperties = "property";
		String pathToUriResources = "resource";
		String pathToUriType = null;
		
		String resourceType = "Property";
		
		if (resourceType.equals("Property")){
			pathToUriType = pathToUriProperties;
		} else if (resourceType.equals("Resource")){
			pathToUriType = pathToUriResources;
		} else {
			throw new IllegalArgumentException("Probably internal error: no resourceType specified");
		}
		
		
		String requestPath = null;
		String requestPathFirstPart = null;
		String requestPathLastPart = null;
		
		
		String resourcePath = null;
		String resourceSubPath = null;
		String[] resourceSubPathArr = null;
		String resourceBase = null;
		String resourceEnding = null;
	
		/* Step 1:
		 *  identify property (query),
		 *  identify if it is bare resource or as a file with ending 
		 *  decide about content type
		 *  if cannot be determined -> error
		 *  */
		
		// get QueryPath
		if(req.getPathInfo() != null && !req.getPathInfo().equals("")){
			requestPath = req.getPathInfo();
		} else {
			requestPath = null;
		}
		
		// split Query Path in its important components
		if (requestPath != null){
			requestPath = requestPath.substring(1); // remove leading slash
			
			if (requestPath.contains("/")){
				
				requestPathFirstPart = requestPath.substring(0, requestPath.lastIndexOf("/"));
				requestPathLastPart = requestPath.substring(requestPath.lastIndexOf("/")+1);
				
				resourceSubPath = requestPathFirstPart;
				resourceSubPathArr = requestPathFirstPart.split("/");
				
			} else {
				
				requestPathFirstPart = null;
				requestPathLastPart = requestPath;
				
				resourceSubPath = null;
				resourceSubPathArr = null;
			
			}
			
			// detect folder structure and put into array
			// maybe resourceBase should be determined with substring lastIndexOf("/"), to determine empty bases
			
			if (requestPathLastPart != null) {
				
				if (requestPathLastPart.contains(".")){
					
					
					resourceBase = requestPathLastPart.substring(0, requestPathLastPart.lastIndexOf("."));
					resourceEnding = requestPathLastPart.substring(requestPathLastPart.lastIndexOf("."));
					
				} else {
					
					resourceBase = requestPathLastPart;
					resourceEnding = null;
					
				}
				
				
			}
		}
		
		resourcePath = pathToUriBase + "/" + pathToUriType + "/";
		if(resourceSubPath != null) {
			resourcePath = resourcePath + resourceSubPath + "/";
		}
		if(resourceBase != null){
			resourcePath = resourcePath + resourceBase;
		}
		
		System.out.println("requestPath: " + requestPath);
		System.out.println("resourcePath: " + resourcePath);
		System.out.println("resourceSubPath: " + resourceSubPath);
		System.out.println("resourceSubPathFragments:");
		if (resourceSubPathArr != null){
		for (String string : resourceSubPathArr) {
			System.out.println("  "+ string);
		}
		}
		System.out.println("resourceBase: " + resourceBase);
		System.out.println("resourceEnding: " + resourceEnding);
		
		
		
		resp.setContentType("application/x-turtle");
			
		String turtleResponse = "<" + resourcePath + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/1999/02/22-rdf-syntax-ns#Property>;";
		resp.setContentType("application/x-turtle");
		resp.getWriter().println(turtleResponse);
		//resp.sendRedirect("/property/" + resourceBase + ".turtle");
		
		
/*		//if ending not found due to file ending
		Boolean useHeaderContentType = false;
		if (resourceEnding == null){
			useHeaderContentType = true;
		} else {
			//if match ending with datatypes (rdf, n3, ...) or (html, ...)
				// try to use mapping that already exists (extra class?)
			//else not matching useHeaderContentType = true;
		}
		
		if (useHeaderContentType){
			//try to find contenttype for response
			//if not html, primarily as rdf/xml - if set also ttl, n3, ...
				// use existing mapping? how to handle content headers
		}
		
		 Step 2:
		 * Read datasets (file - triplestore - sparql) | filter on query or after query for the item as subject
		 * create rdf model
		 * if no data can be found -> error
		 
		
	
		if (resourceBase == null){
			//no real resource addressed, but maybe an overview page?
			
			if (resourceSubPath == null){
				// give error - no resourcebase and no subpath found - no resource can be determined			
				
			} else { // we have most probably an overview page request
				
				// decide for special treaty, e.g. resources/measurements/...
			}
			
		} else {
			//normal resource behaviour
			if (resourceSubPath == null){
				//basic behaviour

			} else {
				// decide for special treaty depending on subPath, e.g. resources/measurements/...
			}
		}
		
		
		 Step 3:
		 * Print data
		 * Step 3.1: RDF formats convert with jena
		 * Step 3.2: HTML format must be processed - add possible alternatives
		 
		
		//if outputFormat equals something like rdf-output -> jena write format
		
		//else if (html) do something more
		
		//else throw error
*/	
	}
}
