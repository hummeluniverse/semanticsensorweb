package net.hummeluniverse.semanticsensorweb.provision;

import java.io.IOException;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class ResourceInformationServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		/*resp.setContentType("text/plain");
		resp.getWriter().println("In a future version of the SemanticSensorWeb-Service, you will be able to acces more detailled information about this resource here. For now only this text shows its presence.");
	*/
	
		/*resp.setContentType("text/plain");
		resp.getWriter().println("In a future version of the SemanticSensorWeb-Service, you will be able to acces more detailled information about this property here. For now only this text shows its presence.");
*/
	/*		List<String> mimeTypesSupported = Arrays.asList(StringUtils.split(
    "application/xbel+xml,text/xml", ','));
String bestMatch = MIMEParse.bestMatch(mimeTypesSupported, "text/*;q=0.5,;q=0.1");*/
	
	
		String pathToUriBase = "http://projects.hummel-universe.net/semanticsensorweb";
		String pathToUriProperties = "property";
		String pathToUriResources = "resource";
		String pathToUriType = null;
		
		String resourceType = "Property";
		
		if (resourceType.equals("Property")){
			pathToUriType = pathToUriProperties;
		} else if (resourceType.equals("Resource")){
			pathToUriType = pathToUriResources;
		} else {
			throw new IllegalArgumentException("Probably internal error: no resourceType specified");
		}
		
		
		String requestPath = null;
		String requestPathFirstPart = null;
		String requestPathLastPart = null;
		
		
		String resourcePath = null;
		String resourceSubPath = null;
		String[] resourceSubPathArr = null;
		String resourceBase = null;
		String resourceEnding = null;
	
		/* Step 1:
		 *  identify property (query),
		 *  identify if it is bare resource or as a file with ending 
		 *  decide about content type
		 *  if cannot be determined -> error
		 *  */
		
		// get QueryPath
		if(req.getPathInfo() != null && !req.getPathInfo().equals("")){
			requestPath = req.getPathInfo();
		} else {
			requestPath = null;
		}
		
		// split Query Path in its important components
		if (requestPath != null){
			requestPath = requestPath.substring(1); // remove leading slash
			
			if (requestPath.contains("/")){
				
				requestPathFirstPart = requestPath.substring(0, requestPath.lastIndexOf("/"));
				requestPathLastPart = requestPath.substring(requestPath.lastIndexOf("/")+1);
				
				resourceSubPath = requestPathFirstPart;
				resourceSubPathArr = requestPathFirstPart.split("/");
				
			} else {
				
				requestPathFirstPart = null;
				requestPathLastPart = requestPath;
				
				resourceSubPath = null;
				resourceSubPathArr = null;
			
			}
			
			// detect folder structure and put into array
			// maybe resourceBase should be determined with substring lastIndexOf("/"), to determine empty bases
			
			if (requestPathLastPart != null) {
				
				if (requestPathLastPart.contains(".")){
					
					
					resourceBase = requestPathLastPart.substring(0, requestPathLastPart.lastIndexOf("."));
					resourceEnding = requestPathLastPart.substring(requestPathLastPart.lastIndexOf("."));
					
				} else {
					
					resourceBase = requestPathLastPart;
					resourceEnding = null;
					
				}
				
				
			}
		}
		
		resourcePath = pathToUriBase + "/" + pathToUriType + "/";
		if(resourceSubPath != null) {
			resourcePath = resourcePath + resourceSubPath + "/";
		}
		if(resourceBase != null){
			resourcePath = resourcePath + resourceBase;
		}
		
		System.out.println("requestPath: " + requestPath);
		System.out.println("resourcePath: " + resourcePath);
		System.out.println("resourceSubPath: " + resourceSubPath);
		System.out.println("resourceSubPathFragments:");
		if (resourceSubPathArr != null){
		for (String string : resourceSubPathArr) {
			System.out.println("  "+ string);
		}
		}
		System.out.println("resourceBase: " + resourceBase);
		System.out.println("resourceEnding: " + resourceEnding);
		
		
		
		
		if(resourceSubPath != null && resourceSubPath.contains("measurement")){
			String turtleResponse = "<" + resourcePath + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://projects.hummel-universe.net/semanticsensorweb/resource/measurement>;";
			resp.setContentType("application/x-turtle");
			resp.getWriter().println(turtleResponse);
			
		} else if (resourceBase.equals("measurement")) {
			String turtleResponse = "<" + resourcePath + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#label> \"Measurement\";";
			resp.setContentType("application/x-turtle");
			resp.getWriter().println(turtleResponse);
			//resp.sendRedirect("/resource/" + resourceBase + ".turtle");
		}
		
		
		
	}
}
