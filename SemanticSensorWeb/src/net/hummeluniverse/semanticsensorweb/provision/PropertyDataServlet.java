package net.hummeluniverse.semanticsensorweb.provision;

import java.io.IOException;
import javax.servlet.http.*;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class PropertyDataServlet extends HttpServlet {
	private final static Logger LOG = Logger.getLogger(PropertyDataServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
		LOG.debug("Servlet started");
	}
}
