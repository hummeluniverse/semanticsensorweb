package net.hummeluniverse.semanticsensorweb.provision;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.hummeluniverse.semanticsensorweb.helper.TimeMeasurement;
import net.hummeluniverse.semanticsensorweb.preparation.CsvSourceManager;
import net.hummeluniverse.semanticsensorweb.preparation.RDFCreator;

import com.hp.hpl.jena.rdf.model.Model;

@SuppressWarnings("serial")
public class BasicQueryServlet extends HttpServlet {
	private final static Logger LOG = Logger.getLogger(BasicQueryServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
			
		long zstVorher;
		CsvSourceManager csvSource = null;
		String config;
		String mode;
		String format;
		Model rdfModel = null;
		
		
		long overallStartTime = TimeMeasurement.printStartMessage("Starting whole RDF-creation process.", LOG);
		LOG.debug("#===================================================#");
		
		//resp.setContentType("text/plain");
		//resp.getWriter().println("Hello, world - Blablabla");
		
		if(req.getParameter("config") == null){
			resp.setStatus(400);
			resp.setContentType("text/html");
			resp.sendRedirect("/basicquery_err400.html");
			
		} else {
		
		// read config for csvSource
		zstVorher = TimeMeasurement.printStartMessage("Starting to read ConfigurationFile.", LOG);
		if (req.getParameter("config") != null) {
			config = req.getParameter("config");
			if (config.equalsIgnoreCase("noaa_metar")) {//TODO remove hardcoded configs
				csvSource = CsvSourceManager.readConfigurationFile("files/config/noaa_metar.properties", true);
			} else if (config.equalsIgnoreCase("noaa_pirep")) {
				csvSource = CsvSourceManager.readConfigurationFile("files/config/noaa_pirep.properties", true);
			} else if (config.equalsIgnoreCase("noaa_metar_oneelement")) {
				csvSource = CsvSourceManager.readConfigurationFile("files/config/noaa_metar_oneelement.properties", true);
			} else {
				try{
					csvSource = CsvSourceManager.readConfigurationFile("files/config/" + config + ".properties", true);
				}
				catch (Exception e){
					e.printStackTrace();
					throw new IllegalArgumentException("Config ('" + config + "') not available. Please choose a correct one. (e.g. noaa_metar)\n " + e.toString());
				}
				
			}
		} else {
			throw new IllegalArgumentException("Please specify a config parameter. e.g. config=metar_noaa");
		}
		
		TimeMeasurement.printStopMessage("Configuration file read", zstVorher, LOG);
		
		// overwrite readMaxEntries Parameter
		String strReadMaxEntries = req.getParameter("readMaxEntries");

		if (strReadMaxEntries != null && !strReadMaxEntries.equals("") && !strReadMaxEntries.equals("")){
			
			LOG.info("readMaxEntries has been manually set to '" + strReadMaxEntries + "'");
			int readMaxEntries;
			try {
				readMaxEntries = Integer.parseInt(strReadMaxEntries);
			} catch (Exception e) {
				throw new IllegalArgumentException("readMaxEntries seems not to be an int value\n " + e.toString());
			}
			csvSource.setReadMaxEntries(readMaxEntries);
		}

		// overwrite attachAdditionalParameters Parameter
		if (req.getParameter("attachAdditionalStatements") != null){
			
			csvSource.setAttachAdditionalStatements(req.getParameter("attachAdditionalStatements"));
			LOG.debug("attachAdditionalStatements has been manually set to '" + csvSource.getAttachAdditionalStatements() + "'");
		}

		zstVorher = TimeMeasurement.printStartMessage("Starting to generate Inputlist.", LOG);
		
		if (req.getParameter("mode") != null){
			mode = req.getParameter("mode");
			LOG.info("mode = " + mode);
			if (mode.equalsIgnoreCase("directConversion")) {
				csvSource.generateInputListFromSourceFile();
			} else if (mode.equalsIgnoreCase("test")) {
				csvSource.generateInputListFromLocalTestFile();
			} else if (mode.equalsIgnoreCase("getConfigFile")) {
				resp.setContentType("text/plain");
				resp.sendRedirect("/files/config/" + config + ".properties");
			} else if (mode.equalsIgnoreCase("getAdditionalStatements")) {
				mode = "getAdditionalStatements";
				// see lower rdfModel = csvSource.getAdditionalStatements();
			} else {
				//mode = "test";
				//csvSource.generateInputListFromLocalTestFile();
				mode = "directConversion";
				csvSource.generateInputListFromSourceFile();
			}
		} else {
			//mode = "test";
			//csvSource.generateInputListFromLocalTestFile();
			mode = "directConversion";
			csvSource.generateInputListFromSourceFile();
			LOG.debug("mode was null and therefore set to: " + mode);
			
		}
		
		TimeMeasurement.printStopMessage("Inputlist created", zstVorher, LOG);
		if (mode.equals("directConversion") || mode.equalsIgnoreCase("test")) {
			LOG.info("Size of InputList:" + csvSource.getInputList().size());
		}
		// read

		zstVorher = TimeMeasurement.printStartMessage("Starting to convert Inputlist to RDF", LOG);
		if (mode.equals("directConversion") || mode.equalsIgnoreCase("test")) {
			rdfModel = RDFCreator.listToRdfModel(csvSource.getInputList(), csvSource.getArrayDescriptions(), csvSource.getNamespaceDefinitions(), csvSource.getAdditionalStatements(), csvSource.getAttachAdditionalStatements());
		} else if (mode.equalsIgnoreCase("getAdditionalStatements")) {
			rdfModel = csvSource.getAdditionalStatements();
		}
		TimeMeasurement.printStopMessage("Inputlist converted to RDF", zstVorher, LOG);
		
		// print to outputstream
		zstVorher = TimeMeasurement.printStartMessage("Starting output for Web", LOG);
		if (rdfModel != null) {
			if (req.getParameter("format") != null) {
				format = req.getParameter("format");
				if (format.equalsIgnoreCase("RDF/XML")) {resp.setContentType("application/rdf+xml"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".rdf\""); rdfModel.write(resp.getWriter(), "RDF/XML"); }
				else if (format.equalsIgnoreCase("RDF/XML-ABBREV")) {resp.setContentType("application/rdf+xml"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".abbrev.rdf\""); rdfModel.write(resp.getWriter(), "RDF/XML-ABBREV");}
				else if (format.equalsIgnoreCase("N-TRIPLE")) {resp.setContentType("text/plain"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".ntriple\""); rdfModel.write(resp.getWriter(), "N-TRIPLE");}
				else if (format.equalsIgnoreCase("TURTLE")) {resp.setContentType("application/x-turtle"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".turtle\""); rdfModel.write(resp.getWriter(), "TURTLE");}
				else if (format.equalsIgnoreCase("TTL")) {resp.setContentType("application/x-turtle"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".ttl\""); rdfModel.write(resp.getWriter(), "TTL");}
				else if (format.equalsIgnoreCase("N3")) {resp.setContentType("text/rdf+n3"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".n3\""); rdfModel.write(resp.getWriter(), "N3");}
				else {resp.setContentType("application/rdf+xml"); resp.addHeader("Content-Disposition", "filename=\"" + config + ".rdf\""); rdfModel.write(resp.getWriter(), "RDF/XML");}
				// resp.addHeader("Content-Disposition", "attachment; filename=\"" + config + ".turtle.txt\"");
			} else {
				resp.setContentType("application/rdf+xml");
				resp.addHeader("Content-Disposition", "filename=\"" + config + ".rdf\"");
				rdfModel.write(resp.getWriter(), "RDF/XML");
			}
		}
		TimeMeasurement.printStopMessage("Output for Web finished", zstVorher, LOG);
		
/*		System.out.println("Starting Output for System");
		zstVorher = System.currentTimeMillis();	
		rdfModel.write(System.out, "Turtle");
		zstNachher = System.currentTimeMillis();
		System.out.println("Output for System finished. (" + ((zstNachher - zstVorher)/1000) + " sec)");*/
		
		//System.out.println(System.getProperty("user.dir"));
		TimeMeasurement.printStopMessage("Overall RDF-Creation Time", overallStartTime, LOG);
	}
		}
}
