/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.provision;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Thomas Hummel
 * @since 13.06.2012
 *
 */
@SuppressWarnings("serial")
public class ResourceDataServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}

}
