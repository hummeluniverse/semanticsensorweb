package net.hummeluniverse.semanticsensorweb.provision;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.rdf.model.Model;

import net.hummeluniverse.semanticsensorweb.helper.DataSource;
import net.hummeluniverse.semanticsensorweb.helper.FileHandler;
import net.hummeluniverse.semanticsensorweb.helper.RuntimeChecker;
import net.hummeluniverse.semanticsensorweb.helper.TimeMeasurement;
import net.hummeluniverse.semanticsensorweb.presentation.kml.KmlConfigManager;
import net.hummeluniverse.semanticsensorweb.presentation.kml.Kml;
import net.hummeluniverse.semanticsensorweb.presentation.kml.KmlCreator;
import net.hummeluniverse.semanticsensorweb.presentation.kml.MapManager;


@SuppressWarnings("serial")
public class BasicKMLServlet extends HttpServlet {
	private final static Logger LOG = Logger.getLogger(BasicKMLServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		long startTime;
		Model rdfModel;
		String mode;
		Boolean testMode = false;
		String configName;
		String urlToSource;
		String sourceFormat;
		String serverAddressPlaceholder = "http://projects.hummel-universe.net/semanticsensorweb";
		String actualServerAddress = "http://" + req.getServerName();
		if (req.getServerName().equals("localhost")){
			actualServerAddress = actualServerAddress + ":" + req.getServerPort();
		}
		
		String pathToNetworkLinks = "kml/networklinks/";
		String fileExtensionNetworkLinks = ".nl.kml";
		
		String pathToKMLConfigs = "kml/config/";
		String fileExtensionKMLConfigs = ".properties";
		
		//System.setProperty( "sun.net.client.defaultConnectTimeout", "100");


		LOG.debug("\n#==============#");
		long overallStartTime = TimeMeasurement.printStartMessage("Starting whole process.", LOG);
		//RuntimeChecker.getHeapStats();


		if(req.getParameter("config") == null){
			resp.setStatus(400);
			resp.setContentType("text/html");
			resp.sendRedirect("/basickml_err400.html");

		} else {
			/*
			 * Read parameters
			 */

			configName = req.getParameter("config"); //TODO accept only correct config-arguments

			if(req.getParameter("mode") != null){
				mode = req.getParameter("mode");
				//TODO accept only correct mode-arguments: showMap, createKml, createNetworkLink
			} else { 
				mode = "createKml";
			}

			String strTestMode = req.getParameter("testMode");
			if (strTestMode != null){
				if (strTestMode.equalsIgnoreCase("on")) {
					testMode = true;
				} else if (strTestMode.equalsIgnoreCase("off")) {
					testMode = false;
				} else if (strTestMode.equalsIgnoreCase("1") || strTestMode.equalsIgnoreCase("true") || strTestMode.equalsIgnoreCase("yes")) {
					testMode = true;
				} else {
					testMode = false;
				}
			}


			if (mode.equalsIgnoreCase("createNetworkLink")) {
				//NetworkLinkManager.checkFileExistance();
				
				if (testMode){
					if (!configName.contains(".test.")){
						configName = configName + ".test";
					}
				}
				
				String pathToNetworkLink = pathToNetworkLinks + configName + fileExtensionNetworkLinks;
				String result = FileHandler.readFileToString(pathToNetworkLink);


				result = KmlConfigManager.replaceServerAdress(result, "<!\\[CDATA\\[" + serverAddressPlaceholder, "<![CDATA[" + actualServerAddress);

				resp.setContentType("application/vnd.google-earth.kml+xml");
				resp.addHeader("Content-Disposition", "filename=\"" + configName + ".nl.kml\"");
				resp.getWriter().write(result);

			} 
			else if (mode.equalsIgnoreCase("getKMLConfigFile")) {
				resp.setContentType("text/plain");
				/*if (testMode){
					if (!configName.contains(".test.")){
						configName = configName + ".test";
					}
				}*/
				resp.sendRedirect(pathToKMLConfigs + configName + fileExtensionKMLConfigs);
				
			}
			else if (mode.equalsIgnoreCase("showMap")){
				LOG.debug("Choice: showMap");
				MapManager config = new MapManager(configName);
				
				String result = FileHandler.readFileToString("map.html");
				
				result = config.replaceHtmlMap(result);
				result = KmlConfigManager.replaceServerAdress(result, serverAddressPlaceholder, actualServerAddress);
				
				resp.setContentType("text/html");
				resp.getWriter().write(result);
				
				//resp.sendRedirect("/map.html");
			}
			else if (mode.equalsIgnoreCase("createKML")) {
				KmlConfigManager config = new KmlConfigManager(configName);


				/*
				 * Getting Sensor RDF Data
				 */

				if (mode.equalsIgnoreCase("createKml") && !testMode){
					
					urlToSource = config.getUrlToSource();
					sourceFormat = config.getSourceFormat();
					
					urlToSource = KmlConfigManager.replaceServerAdress(urlToSource, serverAddressPlaceholder, actualServerAddress);
					
				} else { //testMode
					
					urlToSource = config.getUrlToTestSource();
					sourceFormat = config.getTestSourceFormat();
					
					urlToSource = KmlConfigManager.replaceServerAdress(urlToSource, serverAddressPlaceholder, actualServerAddress);
				
				}
				
				if (!urlToSource.startsWith("http") && !urlToSource.startsWith("ftp")){
					urlToSource = actualServerAddress + "/" + urlToSource;
				}
					
				rdfModel = DataSource.readRdfFile(urlToSource, sourceFormat);
				//RuntimeChecker.getHeapStats();
				
				/*
				 * Getting Additional Datasets
				 */
				ArrayList<Model> additionalRdfModels = KmlCreator.loadAdditionalDatasets(config);
				
                
				/*
				 * Generation Process itself
				 */
				
				//Kml kml = KmlCreator.rdfToKml(config, rdfModel, additionalRdfModels);
				Kml kml = KmlCreator.rdfToKml(config, rdfModel, additionalRdfModels);

				/*
				 * Output
				 */
				startTime = TimeMeasurement.printStartMessage("Starting Output for Web", LOG);
				resp.setContentType("application/vnd.google-earth.kml+xml");
				resp.addHeader("Content-Disposition", "filename=\"" + configName + ".data.kml\"");
				kml.write(resp.getWriter());
				TimeMeasurement.printStopMessage("Output for Web finished", startTime, LOG);
				RuntimeChecker.getHeapStats();


			}
			else {
				resp.setStatus(400);
				resp.setContentType("text/html");
				resp.sendRedirect("/basickml_err400.html");
			}

		}

		TimeMeasurement.printStopMessage("Overall Time", overallStartTime, LOG);
	}
}
