/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

/**
 * @author Thomas Hummel
 * @since 23.04.2012
 *
 */
public class FileHandler {
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */
	/**
	 * System.out.println(System.getProperty("user.dir"));
	 * @param pathToLocalFile Where is the local CSV-File located?
	 * @return returns a reader object of the local file
	 */
	public static Reader readLocalFile(String pathToLocalFile) {
		Reader reader = null;

		if (pathToLocalFile.substring(pathToLocalFile.length()-4).equals(".csv")) {
			try {
				reader = new FileReader(pathToLocalFile);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else if (pathToLocalFile.substring(pathToLocalFile.length()-7).equals(".csv.gz")) {
			try {
				reader = new InputStreamReader(new GZIPInputStream(new FileInputStream(pathToLocalFile)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		return reader;
	}

	
	/**
	 * @param urlToRemoteFile Where is the remote CSV-File located?
	 * @return returns a reader object of the remote file
	 */
	public static InputStreamReader readRemoteFile(String urlToRemoteFile){
		InputStreamReader reader = null;
		
		
		if (urlToRemoteFile.substring(urlToRemoteFile.length()-3).equals("csv")) {
			try {
	            URL url = new URL(urlToRemoteFile);
	            reader = new InputStreamReader(url.openStream());

	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		} else if (urlToRemoteFile.substring(urlToRemoteFile.length()-6).equals("csv.gz")) {
			try {
	            URL url = new URL(urlToRemoteFile);
	            reader = new InputStreamReader(new GZIPInputStream(url.openStream()));

	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		} else {
			throw new IllegalArgumentException("InputFile must be either csv or csv.gz");
		}

		return reader;
	}
	
	/**
	 * @param pathName
	 * @return returns the contents of the file as String
	 */
	public static String readFileToString(String pathName) throws IOException {

	    File file = new File(pathName);
	    StringBuilder fileContents = new StringBuilder((int)file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	    	if (scanner != null){
	    		scanner.close();
	    	}
	        
	    }
	}


}
