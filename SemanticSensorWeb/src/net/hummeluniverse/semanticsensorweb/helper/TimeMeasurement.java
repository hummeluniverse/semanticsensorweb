/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.helper;

import org.apache.log4j.Logger;

/**
 * @author Thomas Hummel
 * @since 27.05.2012
 *
 */
public class TimeMeasurement {
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */
	public static long printStartMessage(String message, Logger log){
		log.debug(message);
		return System.currentTimeMillis();
	}
	
	public static void printStopMessage(String message, long startTime, Logger log){
		log.info(message + "(" + ((System.currentTimeMillis() - startTime)) + "ms)\n");
	}
}
