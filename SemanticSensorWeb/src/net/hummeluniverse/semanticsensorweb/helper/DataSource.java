/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.helper;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;


import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;

/**
 * @author Thomas Hummel
 * @since 27.05.2012
 *
 */
public class DataSource {
	private final static Logger LOG = Logger.getLogger(DataSource.class.getName());
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */
	
	/**
	 * @param sparqlEndpoint The url to the sparql endpoint
	 * @param sparqlQuery The sparql-Query (with CONSTRUCT) as a String
	 * @return returns the Jena-Rdf-Model of this Sparql-Query
	 */	
	 public static Model sparqlQuery(String sparqlEndpoint, String sparqlQuery) {
	     String urlToSparqlEndpoint = sparqlEndpoint;
		 String queryString;
		      
	     if (sparqlQuery == null || sparqlQuery.isEmpty()) {
	    	 
		 queryString = "" + 
		 "CONSTRUCT {\n"+
		 "?airport <http://dbpedia.org/ontology/icaoLocationIdentifier> ?icao. \n" +
		 "?airport <http://dbpedia.org/property/name> ?name. \n" +
		 "?airport <http://dbpedia.org/property/nativename> ?localname. \n" +
		 "}\n"+
		 "WHERE { \n" +
			 "?airport <http://dbpedia.org/ontology/icaoLocationIdentifier> ?icao. \n" +
			 "?airport <http://dbpedia.org/property/name> ?name. \n" +
			 "?airport <http://dbpedia.org/property/nativename> ?localname. \n" +
		 "}";
		 
	     	} else {
	     		queryString = sparqlQuery;
	     	}
	     
	     	
	     	//System.out.println(queryString);
	        
	     	TimeMeasurement.printStartMessage("Starting Sparql-Query (on server: " + urlToSparqlEndpoint + ")", LOG);
	     	Query query = QueryFactory.create(queryString); //s2 = the query above
	     	
	     	LOG.debug("Query: " + query.toString());
	        
	        //QueryExecution qExe = QueryExecutionFactory.sparqlService( urlToSparqlEndpoint, query );
	        
	        //because of setting timeout
	        QueryEngineHTTP qExe=(QueryEngineHTTP) QueryExecutionFactory.sparqlService(urlToSparqlEndpoint,query);
	        qExe.addParam("timeout","5000"); //5 sec
	        
	        
	        //ResultSet results = qExe.execSelect();
	        //ResultSetFormatter.out(System.out, results, query) ;
	        
	        Model results = qExe.execConstruct();
	       //results.write(System.out, "RDF/XML");
	       return results;
	    }

	/**
	 * @param urlToFile The url where to find the source file
	 * @param format The format of the inputFile, e.g. "RDF/XML" or "Turtle"
	 * @return returns the Jena-Rdf-Model of this source
	 */
	public static Model readRdfFile(String urlToFile, String format) {
		Model rdfModel = ModelFactory.createDefaultModel();
		String urlToSource;
		String inputFormat;
		long startTime;
		
		urlToSource = urlToFile;
		
		if (format != null && format != "") {
			inputFormat = format;
		} else {
			inputFormat = "RDF/XML";
		}
		
		startTime = TimeMeasurement.printStartMessage("Starting to read RDF (" + urlToSource + ")", LOG);
		
		URL u = null;
		try {
			u = new URL(urlToSource);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream in = null;
		if (u != null){
			try {
				URLConnection uc = u.openConnection();
				uc.connect();
				int readTimeoutSeconds = 300;
				int readTimeoutMilliseconds = readTimeoutSeconds * 1000;
				uc.setReadTimeout(readTimeoutMilliseconds); //milisec
				in = uc.getInputStream();
			} catch (IOException e){
				e.printStackTrace();
			}
		}

		
        //InputStream in = FileManager.get().open(urlToSource);
        TimeMeasurement.printStopMessage("Reading RDF finished", startTime, LOG);
                        
        startTime = TimeMeasurement.printStartMessage("Starting to convert to RDF Model", LOG);
        rdfModel.read(in, null, inputFormat);
        TimeMeasurement.printStopMessage("Converting into RDF Model finished", startTime, LOG);
		if (in != null){
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return rdfModel;
	}
	}
