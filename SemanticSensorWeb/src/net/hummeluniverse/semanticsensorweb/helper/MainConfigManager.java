/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.helper;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Thomas Hummel
 * @since 09.06.2012
 *
 */
public class MainConfigManager {
	private final static Logger LOG = Logger.getLogger(MainConfigManager.class.getName());
	/* fields */
	protected Properties propertiesFile;
	protected String propertiesFilePath;
	
	/* constructor */
	/**
	 * @param configName Contains the name of the configuration
	 * @param pathToConfigFile represents the path to the configuration file (commonly created out of the configName with the correct path)
	 */
	protected MainConfigManager(String configName, String pathToConfigFile) throws IOException {
		if (configName != null && pathToConfigFile != null){
			BufferedInputStream inputStream = null;
			Properties config = new Properties();

			try {
				inputStream = new BufferedInputStream(new FileInputStream(pathToConfigFile));
				config.load(inputStream);
			}
			catch (IOException e) {
				throw new IOException("Properties File (" + pathToConfigFile + ") could not be found. Please make sure that the config (" + configName + ") exists.\n" +
						"	Maybe it is a NetworkLink-File only?");
			}
			finally {
				if (inputStream != null){
					inputStream.close();
				}

			}

			this.propertiesFilePath = pathToConfigFile;
			this.propertiesFile = config;
		} else {
			//TODO write to logfile?
		}
	}
		
	
	/* basic methods */

	/* other methods */
	/**
	 * Method to read out the simple values in a properties file. Strips leading whitespaces.
	 * @param paramName Name of the parameter in the Properties file
	 * @param defaultValue default Value, if paramName was not found or empty
	 * @return value read or default value
	 */
	protected String readConfigParam(String paramName, String defaultValue){
		String result = null;
		String configValue = this.propertiesFile.getProperty(paramName);
		
		if (configValue != null){
			while(configValue.startsWith(" ")){
				configValue = configValue.substring(1);
			}
		} 
		
		if (configValue != null && !configValue.equals("")) {
			result = configValue;
		} else {
			result = defaultValue;

		}
		
		return result;
		
	}

}
