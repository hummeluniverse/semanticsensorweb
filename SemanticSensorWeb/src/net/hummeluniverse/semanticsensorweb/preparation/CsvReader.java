/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import net.hummeluniverse.semanticsensorweb.helper.FileHandler;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author Thomas Hummel
 * @since 16.04.2012
 *
 */
public class CsvReader {
	private final static Logger LOG = Logger.getLogger(CsvReader.class.getName());

	/* fields */
	
	/* constructor */
	CsvReader() {
		
	}
	
	/* basic methods */
	
	
	/* other methods */
	
	/**
	 * passing data on to the overall file handler
	 * @param pathToLocalFile Where is the local CSV-File located?
	 * @return returns a reader object of the local file
	 */
	public static Reader readLocalFile(String pathToLocalFile) {
		Reader reader = null;
		
		reader = FileHandler.readLocalFile(pathToLocalFile);

/*		try {
			reader = new FileReader(pathToLocalFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		}*/
		
		return reader;
	}
	
	
	/**
	 * passing data on to overall file handler
	 * @param urlToRemoteFile Where is the remote CSV-File located?
	 * @return returns a reader object of the remote file
	 */
	public static InputStreamReader readRemoteFile(String urlToRemoteFile){
		InputStreamReader reader = null;
		
		reader = FileHandler.readRemoteFile(urlToRemoteFile);
		
		return reader;
		
	}
	
	
	/**
	 * @param sourceReader Reader of source-csv-file (eg. new FileReader("filename.csv") or StreamReader, ...)
	 * @param separator Delimiter of the values (e.g. '\t' )
	 * @param quotechar what char is used to mark values as one value (normally "value") (e.g. '\'' )
	 * @param startLine line to start from (e.g. if there is some unused information at the beginning (e.g. 2)
	 * @param stopLine line to stop reading
	 * @return List with the values of csv-file
	 */
	public static List<String[]> convertToList(Reader sourceReader, char separator, char quotechar, int startLine, int stopLine) {
		CSVReader reader = null;
		ArrayList<String[]> myEntries = new ArrayList<String[]>();
		
		try {
			reader = new CSVReader(sourceReader, separator, quotechar, startLine);
			/*myEntries = reader.readAll();*/
			
			// limits lines to 10000
			if (stopLine < 0 || stopLine > 10000){
				stopLine = 100;
			}
			
			int line = 0;
			String[] entry = null;
			while ((entry = reader.readNext()) != null && line < stopLine) {
				myEntries.add(entry);
				line++;
			}

			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOG.debug("Length of arrays in myEntries-list:" + myEntries.get(0).length);
		return myEntries;
	}
	
	public static List<String[]> convertToList(Reader sourceReader, char separator, char quotechar, int startLine) {
		return convertToList(sourceReader, separator, quotechar, startLine, 10000);
	}
	
	/**
	 * Easy conversion without additional params
	 * @param sourceReader Reader of source-csv-file (eg. new FileReader("filename.csv") or StreamReader, ...)
	 * @return List with the values of csv-file
	 */
	public static List<String[]> convertToList(Reader sourceReader) {
		return convertToList(sourceReader, ',', '"', 0);
	}
}
