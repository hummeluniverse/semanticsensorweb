/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.reasoner.IllegalParameterException;
import com.hp.hpl.jena.util.FileManager;

/**
 * @author Thomas Hummel
 * @since 01.05.2012
 *
 */
public class CsvSourceManager {
	/* fields */
	
	// basic file information
	private String nameOfSource;
	private String descriptionOfSource;
	private Boolean localFile; /* local or remote */
	private String pathToFile;
	private String pathToLocalTestFile;
	
	// file structure
	private char separator;
	private char quotechar;
	private int startLine;
	private int readMaxEntries;
	
	// data description
	private List<String[]> inputList;
	private String[][] arrayDescriptions;
	private Model rdfModel;
	
	// additional processing information
	private Map<String, String> namespaceDefinitions;
	private Boolean attachAdditionalStatements;
	private String pathToAdditionalStatements;
	private Model additionalStatements;
	
	
	

	/* constructor */
	CsvSourceManager(String nameOfSource, Boolean localFile, String pathToSource){
		this.setNameOfSource(nameOfSource);
		this.setLocalFile(localFile);
		this.setPathToFile(pathToSource);
	}

	
	
	/* basic methods */
	/**
	 * @return the attachAdditionalStatements
	 */
	public Boolean getAttachAdditionalStatements() {
		return attachAdditionalStatements;
	}


	/**
	 * @param attachAdditionalStatements the attachAdditionalStatements to set
	 */
	public void setAttachAdditionalStatements(Boolean attachAdditionalStatements) {
		if (attachAdditionalStatements.equals("")){
			this.attachAdditionalStatements = true;
		} else {
			this.attachAdditionalStatements = attachAdditionalStatements;
		}
	}
	
	/**
	 * @param strAttachAdditionalStatements the strAttachAdditionalStatements to set
	 */
	public void setAttachAdditionalStatements(String strAttachAdditionalStatements){
		
		if (strAttachAdditionalStatements.equalsIgnoreCase("true") || strAttachAdditionalStatements.equalsIgnoreCase("on") || strAttachAdditionalStatements.equalsIgnoreCase("1")){
			this.setAttachAdditionalStatements(true);
		} else {
			this.setAttachAdditionalStatements(false);
		}
	}
	
	/**
	 * @return the nameOfSource
	 */
	public String getNameOfSource() {
		return nameOfSource;
	}

	/**
	 * @param nameOfSource the nameOfSource to set
	 */
	public void setNameOfSource(String nameOfSource) {
		this.nameOfSource = nameOfSource;
	}

	/**
	 * @return the descriptionOfSource
	 */
	public String getDescriptionOfSource() {
		return descriptionOfSource;
	}

	/**
	 * @param descriptionOfSource the descriptionOfSource to set
	 */
	public void setDescriptionOfSource(String descriptionOfSource) {
		this.descriptionOfSource = descriptionOfSource;
	}

	/**
	 * @return the localFile
	 */
	public Boolean getLocalFile() {
		return localFile;
	}

	/**
	 * @param localFile the localFile to set
	 */
	public void setLocalFile(Boolean localFile) {
			this.localFile = localFile;
	}

	/**
	 * @return the pathToFile
	 */
	public String getPathToFile() {
		return pathToFile;
	}

	/**
	 * @param pathToFile the pathToFile to set
	 */
	public void setPathToFile(String pathToFile) {
		this.pathToFile = pathToFile;
	}

	/**
	 * @return the pathToLocalTestFile
	 */
	public String getPathToLocalTestFile() {
		return pathToLocalTestFile;
	}

	/**
	 * @param pathToLocalTestFile the pathToLocalTestFile to set
	 */
	public void setPathToLocalTestFile(String pathToLocalTestFile) {
		this.pathToLocalTestFile = pathToLocalTestFile;
	}

	/**
	 * @return the separator
	 */
	public char getSeparator() {
		return separator;
	}

	/**
	 * @param separator the separator to set
	 */
	public void setSeparator(char separator) {
		if (separator != ' ') {
			this.separator = separator;
		} else {
			this.separator = ',';
		}
		
	}

	/**
	 * @return the quotechar
	 */
	public char getQuotechar() {
		return quotechar;
	}

	/**
	 * @param quotechar the quotechar to set
	 */
	public void setQuotechar(char quotechar) {
		if (quotechar != ' ') {
			this.quotechar = quotechar;
		} else {
			this.quotechar = '"';
		}
	}

	/**
	 * @return the startLine
	 */
	public int getStartLine() {
		return startLine;
	}

	/**
	 * @param startLine the startLine to set
	 */
	public void setStartLine(int startLine) {
		if (startLine != ' ') {
			this.startLine = startLine;
		} else {
			this.startLine = 0;
		}
	}
	
	/**
	 * @return the readMaxEntries
	 */
	public int getReadMaxEntries() {
		return readMaxEntries;
	}

	/**
	 * @param readMaxEntries the readMaxEntries to set
	 */
	public void setReadMaxEntries(int readMaxEntries) {
		int maxEntriesUpperLimit = 10000;
		if (readMaxEntries > 0 && readMaxEntries <= maxEntriesUpperLimit) {
			this.readMaxEntries = readMaxEntries;
		} 
		else if (readMaxEntries == -1) {
			this.readMaxEntries = maxEntriesUpperLimit;
		}
		else {//0 or any other value
			
			this.readMaxEntries = 100;
		}
	}

	/**
	 * @return the inputList
	 */
	public List<String[]> getInputList() {
		return inputList;
	}

	/**
	 * @param inputList the inputList to set
	 */
	public void setInputList(List<String[]> inputList) {
		this.inputList = inputList;
	}

	/**
	 * @return the arrayDescriptions
	 */
	public String[][] getArrayDescriptions() {
		return arrayDescriptions;
	}

	/**
	 * @param arrayDescriptions the arrayDescriptions to set
	 */
	public void setArrayDescriptions(String[][] arrayDescriptions) {
		this.arrayDescriptions = arrayDescriptions;
	}

	/**
	 * @return the rdfModel
	 */
	public Model getRdfModel() {
		return rdfModel;
	}

	/**
	 * @param rdfModel the rdfModel to set
	 */
	public void setRdfModel(Model rdfModel) {
		this.rdfModel = rdfModel;
	}

	/**
	 * @return the namespaceDefinitions
	 */
	public Map<String, String> getNamespaceDefinitions() {
		return namespaceDefinitions;
	}

	/**
	 * @param namespaceDefinitions the namespaceDefinitions to set
	 */
	public void setNamespaceDefinitions(Map<String, String> namespaceDefinitions) {
		this.namespaceDefinitions = namespaceDefinitions;
	}

	/**
	 * @return the pathToAdditionalStatements
	 */
	public String getPathToAdditionalStatements() {
		return pathToAdditionalStatements;
	}

	/**
	 * @param pathToAdditionalStatements the pathToAdditionalStatements to set
	 */
	public void setPathToAdditionalStatements(String pathToAdditionalStatements) {
		this.pathToAdditionalStatements = pathToAdditionalStatements;
	}

	/**
	 * @return the additionalStatements
	 */
	public Model getAdditionalStatements() {
		return additionalStatements;
	}

	/**
	 * @param additionalStatements the additionalStatements to set
	 */
	public void setAdditionalStatements(Model additionalStatements) {
		this.additionalStatements = additionalStatements;
	}

	/* other methods */
	public static CsvSourceManager readConfigurationFile(String pathToFile, Boolean localFile) throws IOException {
		CsvSourceManager csvSourceManager;
		BufferedInputStream inputStream;
		Properties config = new Properties();
			
		//load file (depending on local/remote file)
		if (localFile = true) {
			 inputStream = new BufferedInputStream(new FileInputStream(pathToFile));
		}
		else { //deprecated
			 inputStream = new BufferedInputStream(new FileInputStream(pathToFile)); //TODO is that correct?
		}
		
		config.load(inputStream);
		inputStream.close();
		
		/* check values */
		
		// entry missing in configfile
		String[] configEntries =  {"configFileValid", "configFileActive", "nameOfSource", "descriptionOfSource", "localFile", "pathToSource",
				"pathToLocalTestFile", "separator", "quotechar", "startLine", "readMaxEntries", "attachAdditionalStatements", "pathToAdditionalStatements", 
				"arrayDescriptions", "namespaceDefinitions"};
		for (String entry : configEntries) {
			if (config.get(entry) == null) {
				throw new IllegalParameterException("Cannot find Entry '" + entry + "' in config file '" + pathToFile + "'.");
			}
		}
		
		// check if configFile is valid and active
		if (!config.get("configFileValid").equals("true")){
			throw new IllegalArgumentException ("Configuration File '" + pathToFile + "' is tagged as invalid.");
		} else if (!config.get("configFileActive").equals("true")) {
			throw new IllegalArgumentException ("Configuration File '" + pathToFile + "' is tagged as inactive.");
		}
		
		// entry empty in config file
		String[] configEntriesNotEmpty = {
				"configFileValid", "configFileActive", "nameOfSource", "pathToSource",
				"pathToAdditionalStatements", "arrayDescriptions", "namespaceDefinitions"
		};
		for (String entry : configEntriesNotEmpty) {
			if (config.get(entry) == null) {
				throw new IllegalParameterException("Entry '" + entry + "' in config file '" + pathToFile + "' should not be empty!");
			}
		}
		
		// line-parameter must be integer - exception thrown by Integer.parseInt
		try {
			Integer.parseInt(config.getProperty("startLine"));
			Integer.parseInt(config.getProperty("readMaxEntries"));
			Boolean.parseBoolean(config.getProperty("attachAdditionalStatements"));
		}
		catch (Exception e){
			try {
				throw new Exception();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
				
		// pathToSource must start with http/https if localFile is not true
		if(!config.getProperty("localFile").equals("true")) {
			String string = config.getProperty("pathToSource");
			if (!string.startsWith("http") && !string.startsWith("https")){
				throw new IllegalArgumentException("Path to a remote source file should start with http or https (config file was '" + pathToFile + "'). Received path is: " + string);
			}
		}
		
		
		/* quotechar quick hack */
		char quotechar = '\u0000';
		try {
		quotechar = config.getProperty("quotechar").charAt(0);
		} catch (Exception e){
			
		}
		 
		/* use values */
		Boolean localFileSource = false;

		if (config.getProperty("localFile").equals("true")){
			localFileSource = true;
		}
		
		
		csvSourceManager = new CsvSourceManager(config.getProperty("nameOfSource"), localFileSource, config.getProperty("pathToSource"));

		csvSourceManager.setDescriptionOfSource(config.getProperty("descriptionOfSource"));
		csvSourceManager.setPathToLocalTestFile(config.getProperty("pathToLocalTestFile"));
		csvSourceManager.setSeparator(config.getProperty("separator").charAt(0));
		csvSourceManager.setQuotechar(quotechar);
		csvSourceManager.setStartLine(Integer.parseInt(config.getProperty("startLine")));
		csvSourceManager.setReadMaxEntries(Integer.parseInt(config.getProperty("readMaxEntries")));
		csvSourceManager.setAttachAdditionalStatements(Boolean.parseBoolean(config.getProperty("attachAdditionalStatements")));
		csvSourceManager.setPathToAdditionalStatements(config.getProperty("pathToAdditionalStatements"));
		
	    //get array split up by the semicolon
		String[] arrayDescriptionBasics = config.getProperty("arrayDescriptions").split(";");
		//create the two dimensional array with correct size
		//String[][] arrayDescriptionDetails =  new String[arrayDescriptionBasics.length][arrayDescriptionBasics.length];
		String[][] arrayDescriptionDetails =  new String[arrayDescriptionBasics.length][3];
		//combine the arrays split by semicolon and comma 
		  for(int i = 0;i < arrayDescriptionDetails.length;i++) {
		      arrayDescriptionDetails[i] = arrayDescriptionBasics[i].split(",");
		  }
		csvSourceManager.setArrayDescriptions(arrayDescriptionDetails);
		
	
	    //get array split up by the semicolon
		String[] namespaceDefinitionBasics = config.getProperty("namespaceDefinitions").split(";");
		//create the two dimensional array with correct size
		HashMap<String, String> namespaceDefinitionDetails =  new HashMap<String, String>();
		//combine the arrays split by semicolon and comma 
		  for(int i = 0;i < namespaceDefinitionBasics.length;i++) {
			  String [] namespaceDefinitionContent = namespaceDefinitionBasics[i].split(",");
			  namespaceDefinitionDetails.put(namespaceDefinitionContent[0], namespaceDefinitionContent[1]);
		  }
		csvSourceManager.setNamespaceDefinitions(namespaceDefinitionDetails);
		
		//TODO check if entries in configfile are correct (e.g. array right amount of items)
		
		//TODO what if something is not set in configfile !! must have correct additional statements turtle file!?
		String pathToAdditionalStatements = config.getProperty("pathToAdditionalStatements");
		if (pathToAdditionalStatements != ""){
			// create an empty model
	        Model model = ModelFactory.createDefaultModel();

	        InputStream in = FileManager.get().open(pathToAdditionalStatements);
	        if (in == null)
	            {
	            throw new IllegalArgumentException( "File: " + pathToAdditionalStatements + " not found");
	        }

	        // read the Turtle file
	        model.read(in, "", "TTL");

	        csvSourceManager.setAdditionalStatements(model);

		}
		
		return csvSourceManager;
	}
	
	public void generateInputListFromSourceFile() throws IOException{
		List<String[]> inputList;
		Reader fileReader;
		if (this.getLocalFile().equals("true")) {
			fileReader = CsvReader.readLocalFile(this.getPathToFile());
		} else {
			fileReader = CsvReader.readRemoteFile(this.getPathToFile());
		}
		
		inputList = CsvReader.convertToList(fileReader, this.getSeparator(), this.getQuotechar(), this.getStartLine(), this.getReadMaxEntries());
		fileReader.close();
		this.setInputList(inputList);
	}
	
	public void generateInputListFromLocalTestFile() throws IOException{
		List<String[]> inputList;
		Reader reader;

		reader = CsvReader.readLocalFile(this.getPathToLocalTestFile());

		
		inputList = CsvReader.convertToList(reader, this.getSeparator(), this.getQuotechar(), this.getStartLine(), this.getReadMaxEntries());
		reader.close();
		this.setInputList(inputList);
	}

}
