/**
 * 
 */
package net.hummeluniverse.semanticsensorweb.preparation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.impl.LiteralImpl;
import com.hp.hpl.jena.vocabulary.RDF;

/**
 * @author Thomas Hummel
 * @since 19.04.2012
 *
 */
public class RDFCreator {
	private final static Logger LOG = Logger.getLogger(RDFCreator.class.getName());
	/* fields */

	/* constructor */

	/* basic methods */

	/* other methods */
	/**
	 * @param inputList List of String Arrays that represent the DataValues (e.g. each represents one line in csv)
	 * @param arrayDescriptions Nested String Array, that describes the structure of the inputList - each value is described by three statements (corresponding property, type of value, additional Information)
	 * @param namespaceDefinitions A Mapping of namespace keys to the full urls for the improvement of the human-readable output
	 * @param additionalStatements any additional rdf statements, for example relations to other models, additional information, ...
	 * @return complete Jena-RDF-Model for further handling
	 */
	public static Model listToRdfModel(List<String[]> inputList, String[][] arrayDescriptions, Map<String, String> namespaceDefinitions, Model additionalStatements, Boolean attachAdditionalStatements) {
		Model rdfModel = ModelFactory.createDefaultModel();
		
		
		// check if inputList exists - we can't work without data
		if (inputList == null) {
			return null;
		} 

		if (attachAdditionalStatements != false) {
			attachAdditionalStatements = true;
		}
		LOG.debug("inputList.size() = " + inputList.size());
		LOG.debug("inputList.get(0).length = " + inputList.get(0).length);
		LOG.debug("arrayDescriptions.length = " + arrayDescriptions.length);
		// check if arrayDescriptions exist - else we handle each value as String-Literal (some Artificial Intelligence could be added if needed)
		if (arrayDescriptions == null || (inputList.get(0).length < arrayDescriptions.length)) {
			arrayDescriptions = new String[inputList.get(0).length][3];

			// fill array with default values - could be overwritten when reading first line of file - not yet implemented
			for (String[] valueDescription : arrayDescriptions) {
				valueDescription[0] = "http://projects.hummel-universe.net/semanticsensorweb/property/hasValue";
				valueDescription[1] = "Literal";
				valueDescription[2] = "plain";
			}

		//check if arrayDescription exists for each Element in an inputList - if not use default one ( TODO could be treated better)
		} else if (inputList.get(0).length > arrayDescriptions.length) {
			String [][] newArrayDescriptions = new String[inputList.get(0).length][3];

			// fill with data that exists
			for (int i = 0; i < arrayDescriptions.length; i++) {
				for (int j = 0; j < 3; j++) {
					if (arrayDescriptions[i][j] != null) {
						newArrayDescriptions[i][j] = arrayDescriptions[i][j];
					} else { // fill missing subelements - alternatively one could call method again
						switch(j) {
						case 0:
							newArrayDescriptions[i][j] = "http://projects.hummel-universe.net/semanticsensorweb/property/hasValue";
							break;
						case 1:
							newArrayDescriptions[i][j] = "Ignore";
							break;
						case 2:
							newArrayDescriptions[i][j] = "Ignore";
							break;
						}
					}
				}
			}

			for (int k = arrayDescriptions.length; k < newArrayDescriptions.length; k++){
				newArrayDescriptions[k][0] = "Ignore";
				newArrayDescriptions[k][1] = "Ignore";
				newArrayDescriptions[k][2] = "Ignore";
			}
			/*for (String[] valueDescription : newArrayDescriptions) {
				valueDescription[0] = "http://projects.hummel-universe.net/semanticsensorweb/property/hasValue";
				valueDescription[1] = "Literal";
				valueDescription[2] = "plain";
			}*/
			arrayDescriptions = newArrayDescriptions;

		//check if arrayDescription has three Values for each Element
		} else {
			for (String[] valueDescription : arrayDescriptions) {
				if (valueDescription[0] == null) {valueDescription[0] = "Ignore"; valueDescription[1] = "Ignore"; valueDescription[2] = "Ignore";}
				if (valueDescription[1] == null) {valueDescription[0] = "Ignore"; valueDescription[1] = "Ignore"; valueDescription[2] = "Ignore";}
				if (valueDescription[2] == null && valueDescription[1] == "Literal") {valueDescription[2] = "plain";}
			}
		}

		// adding additional information, for example namespaces, relations (subPropertyOf), ....
		if(attachAdditionalStatements && additionalStatements!=null){
			rdfModel = additionalStatements.union(rdfModel);
		}
		
		// reading values from inputList
		for (String[] measurementArray : inputList) {
			
			// TODO check if already existing?
			
			// create measurementResource
			Resource measurementResource = rdfModel.createResource("http://projects.hummel-universe.net/semanticsensorweb/resource/measurements/measurement_" + UUID.randomUUID().toString());
			measurementResource.addProperty(RDF.type, rdfModel.createResource("http://projects.hummel-universe.net/semanticsensorweb/resource/measurement"));
			
			for (int i = 0; i < measurementArray.length; i++) {
				String predicateLocalName = arrayDescriptions[i][0];

				String dataValue = measurementArray[i];
				String dataType = arrayDescriptions[i][1];

				String dataAdditionalInformation = arrayDescriptions[i][2];

				if (!dataValue.equals("") && !dataValue.equals("Ignore")) {

					if(dataType.equals("Literal")){
						Property predicate = ResourceFactory.createProperty(predicateLocalName);

						Literal literal = stringToRDFLiteral(dataValue, dataAdditionalInformation);
						//String stringLiteral = dataValue;

						//measurementResource.addProperty(predicate, stringLiteral);
						measurementResource.addProperty(predicate, literal);
					} else if (dataType.equals("Resource")) {

						//object = stringToRDFResource();
						// TODO add object
					} else if (dataType.equals("ComplexResource")) {

						//object = stringToRDFComplexResource();
						// TODO add object
					} else if (dataType.equals("Ignore")) {

						//do not add
					} else {
						// TODO throw Exception

					}

				}
			}
		}
		
		
		// finally we make the output somehow user friendly with better namespaces 
		if (namespaceDefinitions != null) {
			rdfModel.setNsPrefixes(namespaceDefinitions);
		}
		
		
		return rdfModel;
	}

	/**
	 * Converting a given data-Value to an appropriate RDF-Literal
	 * @param dataValue some String that should be the value of the Literal
	 * @param dataAdditionalInformation type of the literal (plain, language-tag like "@en" oder xsd-datatype like "float") 
	 * @return Jena Literal for use in a Jena Model
	 */
	public static Literal stringToRDFLiteral(String dataValue, String dataAdditionalInformation) {
		
		Literal literal;
		
		// XSD Datatypes according http://incubator.apache.org/jena/documentation/notes/typed-literals.html#xsd
		HashMap<String, RDFDatatype> xsdDataTypeMap = new HashMap<String, RDFDatatype>();
		xsdDataTypeMap.put("float", XSDDatatype.XSDfloat);
		xsdDataTypeMap.put("double", XSDDatatype.XSDdouble);
		xsdDataTypeMap.put("int", XSDDatatype.XSDint);
		xsdDataTypeMap.put("long", XSDDatatype.XSDlong);
		xsdDataTypeMap.put("short", XSDDatatype.XSDshort);
		//xsdDataTypeMap.put("dateTime", XSDDatatype.XSDdateTime);
		xsdDataTypeMap.put("datetime", XSDDatatype.XSDdateTime);
		xsdDataTypeMap.put("byte", XSDDatatype.XSDbyte);
		//xsdDataTypeMap.put("unsignedByte", XSDDatatype.XSDunsignedByte);
		xsdDataTypeMap.put("unsignedbyte", XSDDatatype.XSDunsignedByte);
		//xsdDataTypeMap.put("unsignedShort", XSDDatatype.XSDunsignedShort);
		xsdDataTypeMap.put("unsignedshort", XSDDatatype.XSDunsignedShort);
		//xsdDataTypeMap.put("unsignedInt", XSDDatatype.XSDunsignedInt);
		xsdDataTypeMap.put("unsignedint", XSDDatatype.XSDunsignedInt);
		//xsdDataTypeMap.put("unsignedLong", XSDDatatype.XSDunsignedLong);
		xsdDataTypeMap.put("unsignedlong", XSDDatatype.XSDunsignedLong);
		xsdDataTypeMap.put("decimal", XSDDatatype.XSDdecimal);
		xsdDataTypeMap.put("integer", XSDDatatype.XSDinteger);
		//xsdDataTypeMap.put("nonPositiveInteger", XSDDatatype.XSDnonPositiveInteger);
		xsdDataTypeMap.put("nonpositiveinteger", XSDDatatype.XSDnonPositiveInteger);
		//xsdDataTypeMap.put("nonNegativeInteger", XSDDatatype.XSDnonNegativeInteger);
		xsdDataTypeMap.put("nonnegativeinteger", XSDDatatype.XSDnonNegativeInteger);
		//xsdDataTypeMap.put("positiveInteger", XSDDatatype.XSDpositiveInteger);
		xsdDataTypeMap.put("positiveinteger", XSDDatatype.XSDpositiveInteger);
		//xsdDataTypeMap.put("negativeInteger", XSDDatatype.XSDnegativeInteger);
		xsdDataTypeMap.put("negativeinteger", XSDDatatype.XSDnegativeInteger);
		//xsdDataTypeMap.put("Boolean", XSDDatatype.XSDboolean);
		xsdDataTypeMap.put("boolean", XSDDatatype.XSDboolean);
		//xsdDataTypeMap.put("string", XSDDatatype.XSDstring);
		//xsdDataTypeMap.put("normalizedString", XSDDatatype.XSDnormalizedString);
		xsdDataTypeMap.put("normalizedstring", XSDDatatype.XSDnormalizedString);
		//xsdDataTypeMap.put("anyURI", XSDDatatype.XSDanyURI);
		xsdDataTypeMap.put("anyuri", XSDDatatype.XSDanyURI);
		xsdDataTypeMap.put("token", XSDDatatype.XSDtoken);
		//xsdDataTypeMap.put("Name", XSDDatatype.XSDName);
		xsdDataTypeMap.put("name", XSDDatatype.XSDName);
		//xsdDataTypeMap.put("QName", XSDDatatype.XSDQName);
		xsdDataTypeMap.put("qname", XSDDatatype.XSDQName);
		xsdDataTypeMap.put("language", XSDDatatype.XSDlanguage);
		//xsdDataTypeMap.put("NMTOKEN", XSDDatatype.XSDNMTOKEN);
		xsdDataTypeMap.put("nmtoken", XSDDatatype.XSDNMTOKEN);
		//xsdDataTypeMap.put("ENTITIES", XSDDatatype.XSDENTITIES);
		//xsdDataTypeMap.put("NMTOKENS", XSDDatatype.XSDNMTOKENS);
		//xsdDataTypeMap.put("ENTITY", XSDDatatype.XSDENTITY);
		xsdDataTypeMap.put("entity", XSDDatatype.XSDENTITY);
		//xsdDataTypeMap.put("ID", XSDDatatype.XSDID);
		xsdDataTypeMap.put("id", XSDDatatype.XSDID);
		//xsdDataTypeMap.put("NCName", XSDDatatype.XSDNCName);
		xsdDataTypeMap.put("ncname", XSDDatatype.XSDNCName);
		//xsdDataTypeMap.put("IDREF", XSDDatatype.XSDIDREF);
		xsdDataTypeMap.put("idref", XSDDatatype.XSDIDREF);
		//xsdDataTypeMap.put("IDREFS", XSDDatatype.XSDIDREFS);
		//xsdDataTypeMap.put("NOTATION", XSDDatatype.XSDNOTATION);
		xsdDataTypeMap.put("notation", XSDDatatype.XSDNOTATION);
		//xsdDataTypeMap.put("hexBinary", XSDDatatype.XSDhexBinary);
		xsdDataTypeMap.put("hexbinary", XSDDatatype.XSDhexBinary);
		//xsdDataTypeMap.put("base64Binary", XSDDatatype.XSDbase64Binary);
		xsdDataTypeMap.put("base64binary", XSDDatatype.XSDbase64Binary);
		xsdDataTypeMap.put("date", XSDDatatype.XSDdate);
		xsdDataTypeMap.put("time", XSDDatatype.XSDtime);                        
		//xsdDataTypeMap.put("dateTime", XSDDatatype.XSDdateTime);
		xsdDataTypeMap.put("datetime", XSDDatatype.XSDdateTime);
		//xsdDataTypeMap.put("duration", XSDDatatype.XSDduration);
		xsdDataTypeMap.put("duration", XSDDatatype.XSDduration);
		//xsdDataTypeMap.put("gDay", XSDDatatype.XSDgDay);
		xsdDataTypeMap.put("gfay", XSDDatatype.XSDgDay);
		//xsdDataTypeMap.put("gMonth", XSDDatatype.XSDgMonth);
		xsdDataTypeMap.put("gmonth", XSDDatatype.XSDgMonth);
		//xsdDataTypeMap.put("gYear", XSDDatatype.XSDgYear);
		xsdDataTypeMap.put("gyear", XSDDatatype.XSDgYear);
		//xsdDataTypeMap.put("gYearMonth", XSDDatatype.XSDgYearMonth);
		xsdDataTypeMap.put("gyearmonth", XSDDatatype.XSDgYearMonth);
		//xsdDataTypeMap.put("gMonthDay", XSDDatatype.XSDgMonthDay);
		xsdDataTypeMap.put("gmonthday", XSDDatatype.XSDgMonthDay);
		      
		
		dataAdditionalInformation = dataAdditionalInformation.toLowerCase();
		//start with checkn on plain for performance
		if (dataAdditionalInformation == "plain" || dataAdditionalInformation == null){ 
			literal = ResourceFactory.createPlainLiteral(dataValue);
		}
		
		//languages - please start with @
		else if (dataAdditionalInformation.startsWith("@")){ 
			/* Workaround
			 * Easy way with ResourceFactory doesn't work at the moment - see https://issues.apache.org/jira/browse/JENA-213
			 * literal = ResourceFactory.createTypedLiteral(dataValue, dataAdditionalInformation.substring(1));
			 */
			literal = new LiteralImpl( Node.createLiteral(dataValue, dataAdditionalInformation.substring(1), null), null); 
		}
				
		//xsdDatatypes supported
		else if (xsdDataTypeMap.containsKey(dataAdditionalInformation)) { 
			RDFDatatype xsdDataType = xsdDataTypeMap.get(dataAdditionalInformation);
			literal = ResourceFactory.createTypedLiteral(dataValue, xsdDataType);
		} 
		
		/*//manual created datatypes
		else if (false) { 
			// TODO manual created Datatypes
		}*/
		
		//we don't know what to do - maybe note in logfile?
		else { 
			literal = ResourceFactory.createPlainLiteral(dataValue);
		}
		
		return literal;
	}
	
/*	*//**
 *  FileWriter not allowed in Google App Engine
	 * @param rdfModel
	 * @param pathToSaveFile
	 *//*
	public static void rdfFileWriter(Model rdfModel, String pathToSaveFile) {
		File saveFile = new File(pathToSaveFile);
		FileWriterWithEncoding fileWriter;
				
		try {
			fileWriter = new FileWriterWithEncoding(saveFile, "utf-8");
			rdfModel.write(fileWriter, "Turtle");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}*/

}
